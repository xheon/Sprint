#include "DebugBoxRenderComponent.h"


DebugBoxRenderComponent::~DebugBoxRenderComponent()
{
}

void DebugBoxRenderComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void DebugBoxRenderComponent::Create(SBE::GameObject * parent, SBE::GraphicsSystem * graphics, Box * box)
{
	this->parent = parent;
	this->graphics = graphics;
	this->box = box;
	isEnabled = false;

}

void DebugBoxRenderComponent::Update(float dt)
{
	graphics->DrawRect(*box);
}

void DebugBoxRenderComponent::Receive(Message msg)
{
	switch (msg.GetType())
	{

	case MessageType::DEBUG_BOX_ENABLE:
		isEnabled = !isEnabled;
		break;

	default:
		break;
	}

}
