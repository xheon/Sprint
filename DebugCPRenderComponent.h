#pragma once

#include "Component.h"
#include "GraphicsSystem.h"
#include "CheckpointCollision.h"

class DebugCPRenderComponent : public SBE::Component
{
public:
	DebugCPRenderComponent();
	~DebugCPRenderComponent();

	// Inherited via Component
	virtual void Create(SBE::GameObject * parent) override;
	void Create(SBE::GameObject* parent, SBE::GraphicsSystem* graphics, Sprint::CheckpointCollision* cp);
	virtual void Update(float dt) override;

	void Receive(Message msg) override;

private:
	SBE::GraphicsSystem* graphics;
	Sprint::CheckpointCollision* cp;
};

