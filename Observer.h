#ifndef OBSERVER_H
#define OBSERVER_H

#include "Message.h"

namespace SBE
{
	class Observer
	{
	public:
		virtual void Receive(Message msg) = 0;
	};
}

#endif /* OBESRVER_H */