#include "Sprite.h"

#include "Vector.h"

using namespace SBE;

SBE::Sprite::Sprite(GraphicsSystem * graphics) :
	graphics(graphics), alpha_mask(true)
{
}

Sprite::Sprite(GraphicsSystem* graphics, const std::string file) :
	graphics(graphics), alpha_mask(true)
{
	texture = CreateSprite(file);

}

Sprite::Sprite(GraphicsSystem* graphics, const std::string file, bool alpha) :
	graphics(graphics), alpha_mask(alpha)
{
	texture = CreateSprite(file);

}



Sprite::~Sprite()
{
	SDL_DestroyTexture(texture);
}

SDL_Texture* Sprite::CreateSprite(std::string file)
{
	SDL_Surface* surface = SDL_LoadBMP(file.c_str());

	if (surface == nullptr)
	{
		SDL_Log("Unable to load image: %s\n", SDL_GetError());
		return nullptr;
	}

	if (alpha_mask == true)
	{
		Uint32 colorkey = SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF);
		SDL_SetColorKey(surface, SDL_TRUE, colorkey);
	}
	else
	{
		SDL_SetColorKey(surface, SDL_FALSE, 0);
	}

	SDL_Texture* tex = SDL_CreateTextureFromSurface(graphics->renderer, surface);
	SDL_FreeSurface(surface);

	width = 0;
	height = 0;
	SDL_QueryTexture(tex, nullptr, nullptr, &width, &height);


	if (tex == nullptr)
	{
		SDL_Log("Unable to create track texture: %s\n", SDL_GetError());
		return nullptr;
	}

	bounding_box = Box(0, 0, width, height);

	return tex;
}

int Sprite::GetWidth()
{
	return width;
}

int Sprite::GetHeight()
{
	return height;
}

void Sprite::Draw(int x, int y)
{
	Draw(x, y, 0.0f);
}

void Sprite::Draw(int x, int y, float angle)
{
	SDL_Rect rect{ x, y, width, height };
	SDL_SetRenderDrawColor(graphics->renderer, 0xFF, 0xFF, 0xFF, 0xFF);

	SDL_RenderCopyEx(graphics->renderer, texture, nullptr, &rect, angle, nullptr,
		SDL_FLIP_NONE);

	
	// Draw bounding box for debugging purposes
	//bounding_box = Box(x, y, width, height);
	//bounding_box.Rotate(angle);

	//graphics->DrawRect(bounding_box);
}

