#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "Observer.h"
#include "Subject.h"

#include "Vector.h"

#include <vector>

namespace SBE
{
	class Component;
	
	class GameObject : public Observer, public Subject
	{
	public:
		GameObject();
		
		virtual void Create();
		virtual void Update(float dt);

		virtual void Receive(Message msg) override;

		void AddComponent(Component* component);
		

		Vector position;
		float orientation;

		bool isEnable = true;

	protected:
		std::vector<Component*> components;
	};

}

#endif /* GAMEOBJECT_H */