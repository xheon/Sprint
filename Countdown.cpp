#include "Countdown.h"

#include "Component.h"

void Countdown::Reset()
{
	isEnable = true;
	
	for (auto& comp : components)
	{
		comp->Reset();
	}
}
