#include "Car.h"

#include "Component.h"

using namespace Sprint;

Sprint::Car::~Car()
{
}


void Sprint::Car::Create(int player_id)
{
	this->player_id = player_id;
	isEnable = true;

}

void Car::Init()
{

	Reset();
}


void Car::Receive(Message msg)
{
	for (auto& comp : components)
	{
		comp->Receive(msg);
	}
	
	switch (msg.GetType())
	{
	case MessageType::PLAYER_FINISHED:
		Notify(Message{ MessageType::PLAYER_FINISHED, player_id });
		break;

	case MessageType::LAP_FINISHED:
		Notify(Message{ MessageType::LAP_FINISHED });
		break;

	default:
		break;
	}
	/*switch (msg)
	{
	case Message::WALL_HIT:
		position = { 120, 120 };
		SDL_Log("Reset");
		break;

	default:
		break;
	}*/
}

void Car::Reset()
{
	orientation = 0.0f;

	for (auto& comp : components)
	{
		comp->Reset();
	}
}
