#include "CarCollisionComponent.h"

#include "GameObject.h"
#include "Math.h"

#include "SDL.h"

using namespace Sprint;


CarCollisionComponent::~CarCollisionComponent()
{
}

void Sprint::CarCollisionComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void Sprint::CarCollisionComponent::Create(SBE::GameObject * parent, float width, float height)
{
	this->parent = parent;
	bounding_box = Box(parent->position.x, parent->position.y, width, height);
	penalty = 0.5f;
}

void Sprint::CarCollisionComponent::Update(float dt)
{
	bounding_box.MoveTo(parent->position);
	bounding_box.RotateTo(parent->orientation);

	if (penalty > 0)
	{
		penalty -= dt / 1000.0f;
	}
}

void Sprint::CarCollisionComponent::Receive(Message msg)
{
	switch (msg.GetType())
	{
	case MessageType::CAR_HIT:
		CollisionResponse(msg.GetFloat(), msg.GetVector());
		break;

	default:
		break;
	}
}

void Sprint::CarCollisionComponent::CollisionResponse(float overlap, Vector & normal)
{
	// Only do collision response every 0.5 sec
	if (penalty <= 0.0f)
	{
	parent->position = parent->position + normal * overlap;
	/*Vector direction = Vector::ForwardVector(parent->orientation);
		Vector reflection = direction.Reflect(normal);
		float degree = reflection.Orientation();
		parent->orientation = SBE::Math::ClampRotation(degree);

		penalty = 0.5f;
		SDL_Log("Coll response");
*/
		//parent->Receive(Message{ MessageType::CAR_RESPONSE, true });
	}
}


