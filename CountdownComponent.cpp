#include "CountdownComponent.h"

#include "GameObject.h"

CountdownComponent::~CountdownComponent()
{
}

void CountdownComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void CountdownComponent::Create(SBE::GameObject * parent, SBE::UISystem * ui, SBE::TextSprite * text)
{
	this->parent = parent;
	this->ui = ui;
	this->text = text;

	start_time = 4.0f;
	timer = start_time;
}

void CountdownComponent::Update(float dt)
{
	if (timer > 0)
	{
		timer -= dt / 1000.0f;
		int sec = round(timer);
		std::string counter = std::to_string(sec);
		text->ChangeText(counter);
	}
	else
	{
		parent->Notify(Message{ MessageType::GAME_START });
		parent->isEnable = false;
	}
}

void CountdownComponent::Reset()
{
	parent->Notify(Message{ MessageType::GAME_RESTART });
	timer = start_time;
}