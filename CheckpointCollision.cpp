#include "CheckpointCollision.h"

#include "SDL_pixels.h"

using namespace Sprint;

void CheckpointCollision::Create(SBE::GraphicsSystem* graphics,
	SBE::CollisionSystem* collisions, std::vector<Checkpoint> cps)
{
	// Add debug graphics system
	this->graphics = graphics;
	this->collisions = collisions;

	// Init checkpoints
	checkpoints = cps;
	/*checkpoints.emplace_back(Vector{ 406,379 }, Vector{ 406,475 });
	checkpoints.emplace_back(Vector{ 635,371 }, Vector{ 717,453 });
	checkpoints.emplace_back(Vector{ 634,249 }, Vector{ 750,249 });
	checkpoints.emplace_back(Vector{ 633,148 }, Vector{ 739, 88 });
	checkpoints.emplace_back(Vector{ 400,145 }, Vector{ 400, 31 });
	checkpoints.emplace_back(Vector{  84, 59 }, Vector{ 173,149 });
	checkpoints.emplace_back(Vector{  53,251 }, Vector{ 172,251 } );
	checkpoints.emplace_back(Vector{  66,431 }, Vector{ 172,371 } );
	checkpoints.emplace_back(Vector{ 382,379 }, Vector{ 382,475 });*/
}

void CheckpointCollision::DrawCheckpoints()
{
	for (auto& cp : checkpoints)
	{
		graphics->DrawLine(cp.p1, cp.p2, SDL_Color{ 0, 255, 0,0 });
	}
}

int CheckpointCollision::IsHit(const Box& bounding_box)
{
	auto lines = bounding_box.GetLines();
	
	for (auto cp = checkpoints.begin(); cp != checkpoints.end(); ++cp)
	{
		for (auto& line : lines)
		{
			if (collisions->LineLineIntersection(line.first, line.second, cp->p1, cp->p2))
			{
				return std::distance(checkpoints.begin(), cp);
			}
		}
	}
	
	return -1;
}

int Sprint::CheckpointCollision::GetCheckpointsCount() const
{
	return checkpoints.size();
}

Checkpoint* CheckpointCollision::GetCheckkpoint(int index)
{
	if (index < checkpoints.size())
	{
		return &checkpoints[index];
	}

	return nullptr;
}
