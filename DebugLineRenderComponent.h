#pragma once

#include "Component.h"

#include "GraphicsSystem.h"
#include "Box.h"

class DebugLineRenderComponent : public SBE::Component
{
public:
	~DebugLineRenderComponent();

	// Inherited via Component
	virtual void Create(SBE::GameObject * parent) override;
	void Create(SBE::GameObject * parent, SBE::GraphicsSystem* graphics, Vector* p1, Vector* p2);
	virtual void Update(float dt) override;

	virtual void Receive(Message msg) override;


private:
	SBE::GraphicsSystem* graphics;
	Vector* p1;
	Vector* p2;
};

