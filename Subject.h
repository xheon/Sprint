#ifndef SUBJECT_H
#define SUBJECT_H

#include <vector>
#include <algorithm>

#include "Observer.h"

namespace SBE
{
	class Subject
	{
	public:
		void AddReveiver(Observer* observer);
		void DeleteReceiver(Observer* observer);

		void Notify(Message msg);

	protected:
		std::vector<Observer*> receivers;
	};
}

#endif /* SUBJECT_H*/