#ifndef TRACKCOLLISIONCOMPONENT_H
#define TRACKCOLLISIONCOMPONENT_H

#include "Component.h"

#include <vector>

#include "TrackCollision.h"

namespace Sprint
{
	class TrackCollisionComponent : public SBE::Component
	{
	public:
		~TrackCollisionComponent();

		virtual void Create(SBE::GameObject * parent) override;
		virtual void Create(SBE::GameObject* parent, TrackCollision* collision);

		virtual void Update(float dt) override;

		bool OnRoad(float x, float y);

	private:
		TrackCollision* collision;

		Vector position_old;

	};

}

#endif /* TRACKCOLLISIONCOMPONENT_H */
