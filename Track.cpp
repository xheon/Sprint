#include "Track.h"

#include <iostream>

#include "SDL.h"
#include "SDL_surface.h"

#include "CollisionSystem.h"
#include "GraphicsSystem.h"

#include "Component.h"


using namespace Sprint;


void Track::Create(std::string collision_file, 
	SBE::GraphicsSystem* graphics, SBE::CollisionSystem* collisions, std::vector<Checkpoint> cps)
{
	collision_info.Create(collision_file);


	checkpoint_info.Create(graphics, collisions, cps);
}

void Track::Init()
{

}


/*void Track::Update(float dt)
{
	for (const auto& component : components)
	{
		component->Update(dt);
	}

	//checkpoint_info.DrawCheckpoints();
}*/

void Sprint::Track::Receive(Message msg)
{
	for (auto& comp : components)
	{
		comp->Receive(msg);
	}
}

Vector Sprint::Track::GetStartPosition(int index)
{
	// Position players depending on index left from the first checkpoint
	Checkpoint* first = checkpoint_info.GetCheckkpoint(0);
	Vector midpoint = SBE::Math::GetMidpoint(first->p1, first->p2);

	// TODO: Define max players
	int max_players = 4;
	int offset = max_players / 2 - max_players;
	// TODO: Get sprite width
	int width = 12 + 5;
	Vector position;
	position.y = (offset + index) * width + midpoint.y;
	position.x = midpoint.x - 30;

	return position;
}

void Sprint::Track::Reset()
{
	for (auto& comp : components)
	{
		comp->Reset();
	}


}
