#ifndef INPUTMAPPING_H
#define INPUTMAPPING_H

#include <unordered_map>

#include "SDL.h"

#include "Keys.h"

namespace SBE
{

	class InputMapping
	{
	public:
		InputMapping();
		InputMapping(std::unordered_map<SDL_Keycode, Key> mapping);

		bool IsMapped(SDL_Keycode key);
		Key GetAction(SDL_Keycode key);

		bool GetStatus(Key key);
		void SetStatus(Key key, bool status);

		void SetStatus(SDL_Keycode key, bool status);

	private:
		std::unordered_map<SDL_Keycode, Key> mapping;
		std::unordered_map<Key, bool> key_states;
	};

}
#endif /* INPUTMAPPING_H */