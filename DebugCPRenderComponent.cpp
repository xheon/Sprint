#include "DebugCPRenderComponent.h"



DebugCPRenderComponent::DebugCPRenderComponent()
{
}


DebugCPRenderComponent::~DebugCPRenderComponent()
{
}

void DebugCPRenderComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void DebugCPRenderComponent::Create(SBE::GameObject * parent, SBE::GraphicsSystem * graphics, Sprint::CheckpointCollision * cp)
{
	this->parent = parent;
	this->graphics = graphics;
	this->cp = cp;
	isEnabled = false;
}

void DebugCPRenderComponent::Update(float dt)
{
	for (auto& checkpoint : cp->checkpoints)
	{
		graphics->DrawLine(checkpoint.p1, checkpoint.p2, { 0x00, 0xFF, 0xFF, 0xFF });
	}
}

void DebugCPRenderComponent::Receive(Message msg)
{	
	switch (msg.GetType())
	{

	case MessageType::DEBUG_CP_ENABLE:
		isEnabled = !isEnabled;
		break;

	default:
		break;
	}
}
