#ifndef GAME_H
#define GAME_H

#include "GameObject.h"

#include "Engine.h"
#include "GraphicsSystem.h"
#include "CollisionSystem.h"
#include "InputSystem.h"
#include "UISystem.h"

#include "Car.h"
#include "Track.h"
#include "GameInfoUI.h"
#include "TextSprite.h"
#include "WinningScreen.h"
#include "Countdown.h"
#include "PlayerSelection.h"

namespace SBE
{

	class Game : public GameObject
	{
	public:
		~Game();

		void Create() override;
		void Init(Engine* engine);

		void CreateCountdown();

		void Update(float dt) override;
		void Receive(Message msg) override;

		bool IsRunning();
		void Reset();

	private:
		
		void CreateTrack();
		void CreatePlayers();
		void CreateObstacles();
		void CreateUI();
		void CreatePlayerSelection();

		Engine* engine;
		GraphicsSystem* graphics_system;
		CollisionSystem* collision_system;
		InputSystem* input_system;
		UISystem* ui_system;
		
		bool isRunning;
		bool isPaused;
		int max_players;
		int num_players;

		GameInfoUI* ui;
		std::vector<Sprint::Car*> players;
		std::vector<GameObject*> obstacles;
		Sprint::Track track;

		PlayerSelection* selection;
		Countdown* countdown;
		WinningScreen* winning_screen;
		int round;

		int winner_id;
	};

}

#endif /* GAME_ */
