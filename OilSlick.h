#ifndef OILSLICK_H
#define OILSLICK_H

#include "GameObject.h"
namespace Sprint
{
	class OilSlick : public SBE::GameObject
	{
	public:
		~OilSlick();

		// Inherited via GameObject
		virtual void Create() override;
		virtual void Update(float dt) override;
		virtual void Receive(Message msg) override;
	};
}
#endif // !OILSLICK_H


