#ifndef KEYS_H
#define KEYS_H

namespace SBE
{
	enum class Key
	{
		LEFT,
		RIGHT,
		FORWARD,
		BACKWARD,
		RESET,
		NONE
	};
}

#endif /* KEYS_H */