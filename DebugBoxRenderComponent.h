#pragma once

#include "Component.h"

#include "GraphicsSystem.h"
#include "Box.h"

class DebugBoxRenderComponent : public SBE::Component
{
public:
	~DebugBoxRenderComponent();

	// Inherited via Component
	virtual void Create(SBE::GameObject * parent) override;
	void Create(SBE::GameObject * parent, SBE::GraphicsSystem* graphics, Box* box);
	virtual void Update(float dt) override;

	virtual void Receive(Message msg) override;


private:
	SBE::GraphicsSystem* graphics;
	Box* box;
};

