#ifndef UISYSTEM_H
#define UISYSTEM_H

#include "System.h"
#include "GraphicsSystem.h"

#include "SDL_ttf.h"

#include "Sprite.h"

namespace SBE
{
	class UISystem : public System
	{
	public:
		UISystem(std::string font_file, GraphicsSystem* graphics);
		~UISystem() override;

		// Inherited via System
		virtual void Update() override;
		virtual void Receive(Message msg) override;

		GraphicsSystem* graphics;
		TTF_Font* font;
	private:



	};
}

#endif // !UISYSTEM_H


