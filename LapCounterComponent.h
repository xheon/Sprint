#ifndef LAPCOUNTERCOMPONENT_H
#define LAPCOUNTERCOMPONENT_H

#include "Component.h"

namespace Sprint
{
	class LapCounterComponent : public SBE::Component
	{
	public:
		~LapCounterComponent();

		// Inherited via Component
		virtual void Create(SBE::GameObject * parent) override;
		void Create(SBE::GameObject * parent, int max_rounds);
		virtual void Update(float dt) override;
		void Receive(Message msg) override;
		void Reset() override;

	private:
		int max_rounds;
		int lap_counter;
		bool finished;
	};
}
#endif // !LAPCOUNTERCOMPONENT_H


