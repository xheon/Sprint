#include "AIInputComponent.h"

#include <math.h>

#include "Math.h"
#include "CollisionSystem.h"

#include "GameObject.h"
#include "Vector.h"

using namespace Sprint;

AIInputComponent::~AIInputComponent()
{
}

void AIInputComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void AIInputComponent::Create(SBE::GameObject* parent, SBE::GraphicsSystem* graphics, CheckpointCollision* checkpoints)
{
	this->parent = parent;
	this->graphics = graphics;
	this->checkpoints = checkpoints;

	stuck_time = 0;
	distance_old = 0;
	max_stuck_time = 0.5f;

	steer_deadzone = 5.0f;

	SetNextTarget(0);
	max_distance = (parent->position - current_target).Length();
}

void AIInputComponent::Receive(Message msg)
{
	switch (msg.GetType())
	{
	case MessageType::CHECKPOINT_HIT:
	{
		int index = GetNextCheckpoint(msg.GetInt());
		current_cp_index = index;
		SetNextTarget(index);
		max_distance = (parent->position - current_target).Length();
		break;
	}

	case MessageType::GAME_RESTART:
		isEnabled = false;
		break;

	case MessageType::GAME_START:
		isEnabled = true;
		break;

	default:
		break;
	}
}

void Sprint::AIInputComponent::Reset()
{
	SetNextTarget(0);
	int current_cp_index = 0;
}

void AIInputComponent::Update(float dt)
{	
	// Check if I'm stuck for a certain amount of time
	if (stuck_time > max_stuck_time)
	{
		SetNextTarget(current_cp_index);
		stuck_time = 0;
		SDL_Log("Try new target");
	}

	// Get center of the car
	center = parent->position + Vector{ 12, 6 };
	
	// Calculate distance from the car to the target
	Vector difference = current_target - center;
	float distance = difference.Length();

	// Get direction to the target
	float degree = difference.Orientation();

	float delta = SBE::Math::ClampRotation(degree - parent->orientation);

	// Determine to steer left or right depending on the angle to the target
	// Reset steering
	parent->Receive(Message{ MessageType::STEER_RIGHT, false });
	parent->Receive(Message{ MessageType::STEER_LEFT, false });

	// Only steer if the angle is bigger than the deadzone
	if (delta > 180 && 360 - delta > steer_deadzone)
	{
		parent->Receive(Message{ MessageType::STEER_LEFT, true });
	}
	else if (delta < 180 && delta > steer_deadzone)
	{
		parent->Receive(Message{ MessageType::STEER_RIGHT, true });
	}

	// Accelerate anyways
	//float abs_delta = difference.OrientationAbs();
	//SDL_Log("Break, %f", abs_delta);
	if (velocity->Length() > 150 && distance < 50)
	{
		parent->Receive(Message{ MessageType::ACCELERATE, false });
		parent->Receive(Message{ MessageType::BREAK, true });
		//SDL_Log("Break, %f", velocity->Length());
	}
	else
	{
		parent->Receive(Message{ MessageType::BREAK, false });
		parent->Receive(Message{ MessageType::ACCELERATE, true });
		//SDL_Log("Acc, %f", velocity->Length());
	}


	// Assume I'm stuck
	if (abs(distance_old - distance) <= 0.00)
	{
		stuck_time += dt / 1000.0f;
	}

	distance_old = distance;

	// Draw line to active checkpoint
	//graphics->DrawLine(position, current_target, { 0x00, 0xFF, 0x00, 0xFF });
}

int Sprint::AIInputComponent::GetNextCheckpoint(int index)
{
	int next = (index + 1) % checkpoints->GetCheckpointsCount();
	return next;
}

void Sprint::AIInputComponent::SetNextTarget(int index)
{
	// Use random position around midpoint of checkpoint
	float random = rand() / static_cast<float>(RAND_MAX);
	random = random * 2 - 1; // Transform [-1,1]

	float range = 0.6f; // Fraction of the original length

	// Get the checkpoint
	auto cp = checkpoints->GetCheckkpoint(index);

	// Get the midpoint of the checkpoint
	Vector mid = SBE::Math::GetMidpoint(cp->p1, cp->p2);

	// Calculate the direction vector of the checkpoint
	Vector difference = Vector{ cp->p1 - cp->p2 };
	Vector dir = difference.Normalize();

	// Get length from the midpoint to the end of the checkpoint
	// Which is the half_length of the line between the checkpoint points
	float half_length = difference.Length() / 2;

	// Calculate the offset in around the midpoint
	Vector offset =  dir * range * random * half_length;

	current_target = mid + offset;
}

