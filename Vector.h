#ifndef POINT_H
#define POINT_H

class Vector
{
public:
	Vector();
	Vector(float x, float y);
	Vector(double x, double y);
	Vector(int x, int y);

	float Length() const;
	float Orientation() const;
	float OrientationAbs() const;
	void Rotate(float rad);

	static Vector ForwardVector(float degree);
	static Vector RotatePoint(Vector& point, float angle, Vector& center);
	static Vector HeadingVector(float angle);

	Vector Normalize() const;
	Vector Reflect(const Vector& n) const;
	Vector Abs() const;

	float Dot(Vector const & rhs) const;
	
	float x;
	float y;


	// Operators
	Vector operator- (Vector const& rhs) const;
	Vector operator+ (Vector const& rhs) const;
	Vector operator* (float const& scalar) const;
	Vector operator/ (float const& scalar) const;
	Vector operator* (Vector const& rhs) const;
	Vector operator/ (Vector const& rhs) const;
};

#endif /* POINT_H */

