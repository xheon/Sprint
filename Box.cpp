#include "Box.h"

#include "SDL.h"

#include "Math.h"

Box::Box()
{
}

Box::Box(float x, float y, float width, float height)
{
	// 1px correction so the lines are outside of the box
	points.emplace_back(x, y);
	points.emplace_back(x + width, y);
	points.emplace_back(x, y + height);
	points.emplace_back(x + width, y + height);
}


Box::Box(Vector p1, Vector p2, Vector p3, Vector p4)
{
	points.push_back(p1);
	points.push_back(p2);
	points.push_back(p3);
	points.push_back(p4);
}

void Box::Rotate(float degree)
{
	Vector center = (points[0] + points[3]) / 2;

	for (auto& p : points)
	{
		p = Vector::RotatePoint(p, degree, center);
	}
}

void Box::RotateTo(float degree)
{
	// Get the orientation of the box
	auto axis = points[1] - points[0];
	float angle = axis.Orientation();

	// Get the delta rotation
	float diff = SBE::Math::ClampRotation(degree - angle);

	
	if (abs(diff) > 0.1f)
	{
		Rotate(diff);
	}
}

void Box::MoveTo(const Vector & position)
{
	// Get the position of the center
	auto pos = (points[0] + points[3]) / 2;
	auto offset = Vector{ GetWidth() / 2, GetHeight() / 2 };
	pos = pos + offset;
	auto dif = position - pos;

	if (dif.Length() > 0.1f)
	{
		for (auto& p : points)
		{
			p = p + dif + offset * 2;
		}
	}
}

float Box::GetHeight() const
{
	auto v = (points[2] - points[0]);
	return v.Length();
}

float Box::GetWidth() const
{
	auto v = (points[1] - points[0]);
	return v.Length();
}

const Vector& Box::GetPoint(int index) const
{
	return points[index];
}

const std::vector<std::pair<const Vector&, const Vector&>> Box::GetLines() const
{
	std::vector<std::pair<const Vector&, const Vector&>> lines;

	lines.emplace_back(GetPoint(0), GetPoint(1));
	lines.emplace_back(GetPoint(1), GetPoint(3));
	lines.emplace_back(GetPoint(2), GetPoint(3));
	lines.emplace_back(GetPoint(0), GetPoint(2));

	return lines;
}

const std::vector<Vector>& Box::GetPoints() const
{
	return points;
}

std::pair<float, float> Box::Project(const Vector & axis) const
{
	std::pair<float, float> proj;
	
	proj.first = axis.Dot(points[0]);
	proj.second = proj.first;

	// Loop over all vertices to find min and max extend
	for (auto& p : points)
	{
		float value = axis.Dot(p);

		if (value < proj.first)
		{
			proj.first = value;
		}
		else if (value > proj.second)
		{
			proj.second = value;
		}
	}
	
	return proj;
}


