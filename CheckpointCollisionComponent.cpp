#include "CheckpointCollisionComponent.h"

#include "SDL.h"

#include "GameObject.h"

using namespace Sprint;

CheckpointCollisionComponent::~CheckpointCollisionComponent()
{
}

void CheckpointCollisionComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void CheckpointCollisionComponent::Create(SBE::GameObject * parent, CheckpointCollision* checkpoints,
	Box* bounding_box)
{
	this->parent = parent;
	this->checkpoints = checkpoints;
	this->bounding_box = bounding_box;

	// Init checkpoint flags to false
	checkpoints_flags.resize(checkpoints->GetCheckpointsCount(), false);
}

void Sprint::CheckpointCollisionComponent::Reset()
{
	for (auto& cp : checkpoints_flags)
	{
		cp = false;
	}
}

void CheckpointCollisionComponent::Update(float dt)
{
	int checkpoint_index = checkpoints->IsHit(*bounding_box);

	if (checkpoint_index > -1)
	{
		bool set = SetCheckpoint(checkpoint_index);

		if (set && CheckLap())
		{
			parent->Receive(Message{ MessageType::LAP_FINISHED });
			//SDL_Log("Finished one lap\n");

			ResetCheckpoints();
		}
	}

}

bool CheckpointCollisionComponent::CheckLap()
{
	bool lap = true;

	for (const auto& cp : checkpoints_flags)
	{
		if (cp == false)
		{
			lap = false;
		}
	}

	return lap;
}

void CheckpointCollisionComponent::ResetCheckpoints()
{
	for (auto& cp : checkpoints_flags)
	{
		cp = false;
	}
}


bool CheckpointCollisionComponent::SetCheckpoint(int index)
{
	// Get last checkpoint
	int last = -1;
	for (auto cp = checkpoints_flags.begin(); cp != checkpoints_flags.end();
		++cp)
	{
		if (*cp == true)
		{
			last = std::distance(checkpoints_flags.begin(), cp);
		}
	}

	if (index == last + 1 && checkpoints_flags[index] == false)
	{
		checkpoints_flags[index] = true;

		Message msg{ MessageType::CHECKPOINT_HIT, index };
		parent->Receive(msg);

		SDL_Log("Checkpoint %d\n", index);
		return true;
	}
	else
	{
		return false;
	}
}

