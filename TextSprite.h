#ifndef TEXTSPRITE_H
#define TEXTSPRITE_H

#include "Sprite.h"

#include "UISystem.h"

namespace SBE
{
	class TextSprite : public Sprite
	{
	public:
		TextSprite(UISystem* ui, std::string text);
		TextSprite(UISystem* ui, std::string text, SDL_Color color);
		~TextSprite();

		SDL_Texture* CreateTexture(const std::string& text);

		void ChangeText(const std::string& text);
		void ChangeColor(const SDL_Color color);

	private:
		UISystem* ui;
		
		std::string text;
		int font_size;
		SDL_Color font_color;
	};

}
#endif // !TEXTSPRITE_H

