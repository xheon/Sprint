#ifndef ENGINE_H
#define ENGINE_H

#include <map>

#include "SDL.h"

#include "System.h"

namespace SBE
{

	class Engine
	{
	public:
		bool Init();
		void Destroy();

		void Update();
		void UpdateDeferred();
		bool IsRunning();

		void AddSystem(System* system);
		void AddSystem(System* system, bool deferred);

		//System* GetSystem(std::string);

		int GetElapsedTime();

		SDL_Window* window;
		SDL_Renderer* renderer;

	private:
		//std::map<std::string, System*> systems;
		std::vector<std::pair<System*, bool>> systems;
		bool isRunning;
	};

}

#endif // ENGINE_H