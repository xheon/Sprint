#include "InputMapping.h"

using namespace SBE;

SBE::InputMapping::InputMapping()
{
}

InputMapping::InputMapping(std::unordered_map<SDL_Keycode, Key> mapping)
	: mapping(mapping)
{
	// Create action map
	for (auto& action : mapping)
	{
		key_states.emplace(action.second, false);
	}
}

bool InputMapping::IsMapped(SDL_Keycode key)
{
	if (mapping.find(key) != mapping.end())
	{
		return true;
	}

	return false;
}

Key InputMapping::GetAction(SDL_Keycode key)
{
	auto element = mapping.find(key);

	if (element != mapping.end())
	{
		return mapping.find(key)->second;
	}

	return Key::NONE;
}

bool InputMapping::GetStatus(Key key)
{
	auto element = key_states.find(key);

	if (element != key_states.end())
	{
		return element->second;
	}

	return false;
}

void InputMapping::SetStatus(Key key, bool status)
{
	auto& element = key_states.find(key);

	if (element != key_states.end())
	{
		element->second = status;
	}
}

void InputMapping::SetStatus(SDL_Keycode key, bool status)
{
	if (IsMapped(key))
	{
		Key action = GetAction(key);

		SetStatus(action, status);
	}
}

