#include "DebugLineRenderComponent.h"


DebugLineRenderComponent::~DebugLineRenderComponent()
{
}

void DebugLineRenderComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void DebugLineRenderComponent::Create(SBE::GameObject * parent, SBE::GraphicsSystem * graphics, Vector* p1, Vector* p2)
{
	this->parent = parent;
	this->graphics = graphics;
	this->p1 = p1;
	this->p2 = p2;
	isEnabled = false;
}

void DebugLineRenderComponent::Update(float dt)
{
	graphics->DrawLine(*p1, *p2, { 0, 0xFF, 0xFF, 0xFF });
}

void DebugLineRenderComponent::Receive(Message msg)
{
	switch (msg.GetType())
	{

	case MessageType::DEBUG_AI_ENABLE:
		isEnabled = !isEnabled;
		break;

	default:
		break;
	}

}
