#ifndef CAR_H
#define CAR_H

#include "GameObject.h"

#include "SDL.h"

namespace Sprint
{

	class Car : public SBE::GameObject
	{
	public:
		virtual ~Car();
		
		void Create(int player_id);
		void Init();

		void Receive(Message msg) override;

		void Reset();
		
		int player_id;
	private:
	};

}

#endif /* CAR_H */