#ifndef AIINPUTCOMPONENT_H
#define AIINPUTCOMPONENT_H

#include "Component.h"

#include <vector>

#include "SDL.h"
#include "GraphicsSystem.h"

#include "Checkpoint.h"
#include "CheckpointCollision.h"

namespace Sprint
{
	class AIInputComponent : public SBE::Component, public SBE::Observer
	{
	public:
		~AIInputComponent();

		void Create(SBE::GameObject* parent) override;
		void Create(SBE::GameObject* parent, SBE::GraphicsSystem* graphics, CheckpointCollision* checkpoints);
		void Receive(Message msg) override;
		void Reset() override;

		void Update(float dt) override;

		int GetNextCheckpoint(int index);
		void SetNextTarget(int index);
		Vector* velocity;
		Vector center;

		Vector current_target;

	private:
		CheckpointCollision* checkpoints;
		//Checkpoint* current_checkpoint;
		SBE::GraphicsSystem* graphics;

		int current_cp_index;
		float stuck_time;
		float max_stuck_time;
		float distance_old;
		float max_distance;

		float steer_deadzone;
	};

}

#endif /* AIINPUTCOMPONENT_H */
