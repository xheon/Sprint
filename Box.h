#ifndef BOX_H
#define BOX_H

#include <vector>

#include "Vector.h"

class Box
{
public:
	Box();
	Box(float x, float y, float width, float height);
	Box(Vector p1, Vector p2, Vector p3, Vector p4);

	void Rotate(float degree);
	void RotateTo(float degree);
	void MoveTo(const Vector& position);

	float GetHeight() const;

	float GetWidth() const;

	const Vector& GetPoint(int index) const;

	const std::vector<std::pair<const Vector&, const Vector&>> GetLines() const;
	const std::vector<Vector>& GetPoints() const;

	std::pair<float, float> Project(const Vector& axis) const;

private:
	std::vector<Vector> points;
};

#endif /* BOX_H */