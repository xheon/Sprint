#ifndef PLAYERINPUTCOMPONENT_H
#define PLAYERINPUTCOMPONENT_H

#include "Component.h"
#include "Observer.h"
#include "InputMapping.h"

#include "Vector.h"

namespace Sprint
{
	class PlayerInputComponent : public SBE::Component, public SBE::Observer
	{
	public:
		~PlayerInputComponent();

		void Create(SBE::GameObject* parent) override;
		void Create(SBE::GameObject* parent, SBE::InputMapping* mapping);

		void Update(float dt) override;


	private:
		SBE::InputMapping* input;

		// Inherited via Observer
		virtual void Receive(Message msg) override;
	};
}

#endif /* PLAYERINPUTCOMPONENT_H */