#ifndef RENDERCOMPONENT_H
#define RENDERCOMPONENT_H 

#include "Component.h"

#include "GraphicsSystem.h"

#include "Sprite.h"

namespace Sprint
{
	class RenderComponent : public SBE::Component
	{
	public:
		~RenderComponent();

		void Create(SBE::GameObject* parent) override;
		void Create(SBE::GameObject* parent, SBE::GraphicsSystem* graphics,
			std::string sprite_name);

		void Create(SBE::GameObject * parent, SBE::GraphicsSystem * graphics, std::string sprite_name, bool alpha);
		void Create(SBE::GameObject * parent, SBE::GraphicsSystem * graphics, SBE::Sprite* sprite);
		void Update(float dt) override;

		int GetWidth();
		int GetHeight();

		SBE::Sprite* sprite;
		Vector offset;


	private:
		SBE::GraphicsSystem* graphics;
	};

}
#endif /* RENDERCOMPONENT_H */