#pragma once
#include "Component.h"

#include "TextSprite.h"
class CountdownComponent : public SBE::Component
{
public:
	~CountdownComponent();

	// Inherited via Component
	virtual void Create(SBE::GameObject * parent) override;
	void Create(SBE::GameObject* parent, SBE::UISystem* ui, SBE::TextSprite* text);
	virtual void Update(float dt) override;

	void Reset();

private:
	SBE::UISystem* ui;
	SBE::TextSprite* text;
	float start_time;
	float timer;

	
};

