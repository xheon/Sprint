#ifndef COMPONENT_H
#define COMPONENT_H

#include "Message.h"

namespace SBE
{
	class GameObject;

	class Component
	{
	public:
		virtual ~Component() {}

		virtual void Create(GameObject* parent) = 0;
		virtual void Update(float dt) = 0;
		virtual void Receive(Message msg) {}
		virtual void Reset() {}

		bool isEnabled = true;
	protected:
		GameObject* parent;
	};
}

#endif /* COMPONENT_H */