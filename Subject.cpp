#include "Subject.h"

using namespace SBE;

void Subject::AddReveiver(Observer* observer)
{
	receivers.push_back(observer);
}

void Subject::DeleteReceiver(Observer* observer)
{
	receivers.erase(std::remove(receivers.begin(), receivers.end(), observer), receivers.end());
}

void Subject::Notify(Message msg)
{
	for (auto& r : receivers)
	{
		r->Receive(msg);
	}
}