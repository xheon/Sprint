#include "PlayerInputComponent.h"

#include "GameObject.h"

#include "Math.h"

using namespace Sprint;

PlayerInputComponent::~PlayerInputComponent()
{
}

void PlayerInputComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void PlayerInputComponent::Create(SBE::GameObject * parent, SBE::InputMapping * mapping)
{
	this->parent = parent;
	this->input = mapping;
}

void PlayerInputComponent::Update(float dt)
{	
	parent->Receive(Message{ MessageType::STEER_LEFT, input->GetStatus(SBE::Key::LEFT) });
	parent->Receive(Message{ MessageType::STEER_RIGHT, input->GetStatus(SBE::Key::RIGHT) });

	parent->Receive(Message{ MessageType::ACCELERATE, input->GetStatus(SBE::Key::FORWARD) });
	parent->Receive(Message{ MessageType::BREAK, input->GetStatus(SBE::Key::BACKWARD) });
}

void Sprint::PlayerInputComponent::Receive(Message msg)
{
	switch (msg.GetType())
	{
	case MessageType::GAME_START:
		isEnabled = true;
		break;

	case MessageType::GAME_RESTART:
		isEnabled = false;
		break;

	default:
		break;
	}
}
