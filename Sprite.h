#ifndef SPRITE_H
#define SPRITE_H

#include <string>
#include <vector>

#include "SDL.h"

#include "GraphicsSystem.h"

#include "Vector.h"

namespace SBE
{

	class Sprite
	{
	public:
		Sprite(GraphicsSystem* graphics);
		Sprite(GraphicsSystem* graphics, std::string file);
		Sprite(GraphicsSystem* graphics, std::string file, bool alpha);
		~Sprite();

		SDL_Texture * CreateSprite(const std::string file);

		void Draw(int x, int y);
		void Draw(int x, int y, float angle);

		int GetWidth();
		int GetHeight();

		Box bounding_box;

		bool alpha_mask;

	protected:
		GraphicsSystem* graphics;
		SDL_Texture* texture;


		int width;
		int height;

	};

}

#endif /* SPRITE_H*/
