#ifndef GRAPHICSSYSTEM_H
#define GRAPHICSSYSTEM_H

#include "System.h"
#include "SDL.h"

#include "Box.h"

#include "Vector.h"

namespace SBE
{

	class GraphicsSystem : public System
	{
	public:
		GraphicsSystem(SDL_Renderer* renderer);
		~GraphicsSystem() override;
		
		//Sprite* CreateSprite(std::string file);

		void Update() override;
		void Receive(Message msg) override;

		void DrawLine(const Vector& p1, const Vector& p2);
		void DrawLine(const Vector& p1, const Vector& p2, const SDL_Color& color);

		void DrawRect(const Vector & p1, const Vector & p2, const Vector & p3, const Vector & p4);
		void DrawRect(Box& box);

	//private:
		SDL_Renderer* renderer;
	};

}

#endif /* GRAPHICSSYSTEM_H */