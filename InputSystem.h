#ifndef INPUTSYSTEM_H
#define INPUTSYSTEM_H

#include <unordered_map>
#include <vector>

#include "SDL.h"

#include "System.h"
#include "InputMapping.h"
#include "Keys.h"


namespace SBE
{
	enum class Window
	{
		CLOSE
	};

	class InputSystem : public System
	{
	public:
		//(std::unordered_map<SDL_Keycode, Key> mapping);
		~InputSystem() override;

		void Update() override;
		void Receive(Message msg) override;

		void AddMapping(InputMapping* mapping);
		InputMapping* GetMapping(int index);
		int GetMappingsCount() const;
		std::vector<InputMapping*>& GetMappings();

		bool GetStatus(Window window);

	private:
		std::vector<InputMapping*> mappings;
		
		//std::unordered_map<Key, bool> keyboard;
		std::unordered_map<Window, bool> window;

		//std::unordered_map<SDL_Keycode, Key> mapping;
	};

}

#endif /* INPUTSYSTEM_H */
