#include "GameInfoUI.h"

#include "Component.h"

GameInfoUI::~GameInfoUI()
{
}

void GameInfoUI::Reset()
{
	for (auto& comp : components)
	{
		comp->Reset();
	}
}
