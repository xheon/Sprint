#ifndef CHECKPOINTCOLLISIONCOMPONENT_H
#define CHECKPOINTCOLLISIONCOMPONENT_H

#include "Component.h"

#include <vector>

#include "Vector.h"
#include "CheckpointCollision.h"

namespace Sprint
{

	class CheckpointCollisionComponent : public SBE::Component
	{
	public:
		~CheckpointCollisionComponent();

		void Create(SBE::GameObject * parent) override;
		void Create(SBE::GameObject * parent, CheckpointCollision* checkpoints, Box* bounding_box);
		void Reset() override;

		virtual void Update(float dt) override;


	private:
		CheckpointCollision* checkpoints;
		std::vector<bool> checkpoints_flags;
		Box* bounding_box;


		bool SetCheckpoint(int index);
		
		bool CheckLap();
		void ResetCheckpoints();

		
	};

}
#endif /* CHECKPOINTCOLLISIONCOMPONENT_H */