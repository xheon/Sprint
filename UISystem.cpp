#include "UISystem.h"

SBE::UISystem::UISystem(std::string font_file, GraphicsSystem * graphics) :
	graphics(graphics)
{
	TTF_Init();
	font = TTF_OpenFont(font_file.c_str(), 12); //this opens a font style and sets a size
	if (font == NULL)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "font cannot be created! SDL_Error: %s\n", SDL_GetError());
	}
}

SBE::UISystem::~UISystem()
{
	TTF_CloseFont(font);
	TTF_Quit();
}

void SBE::UISystem::Update()
{
}

void SBE::UISystem::Receive(Message msg)
{
}
