#include <iostream>

#include "Engine.h"
#include "Track.h"
#include "Car.h"

#include "InputSystem.h"
#include "GraphicsSystem.h"

#include "Game.h"

int main(int argc, char** argv)
{
	SBE::Engine engine;
	engine.Init();
	
	SBE::Game game; 
	game.Create();
	game.Init(&engine);

	int last_time = engine.GetElapsedTime();
	while (game.IsRunning())
	{
		int new_time = engine.GetElapsedTime();
		int dt = new_time - last_time;

		game.Update(dt);


		int delay = 16 - dt;
		if (delay >= 0)
		{
			SDL_Delay(delay);
		}

		last_time = new_time;
	}


	engine.Destroy();

	//system("pause");

	return 0;
}