#include "TextSprite.h"

using namespace SBE;

TextSprite::TextSprite(UISystem * ui, std::string text) :
	ui(ui), Sprite(ui->graphics), font_color(SDL_Color{ 0xff, 0xff, 0xff }),
	text(text)
{
	texture = CreateTexture(text);
}

SBE::TextSprite::TextSprite(UISystem * ui, std::string text, SDL_Color color) :
	ui(ui), Sprite(ui->graphics), font_color(color), text(text)
{
	texture = CreateTexture(text);
}

TextSprite::~TextSprite()
{
}

SDL_Texture * SBE::TextSprite::CreateTexture(const std::string & text)
{
	if (texture != nullptr)
	{
		SDL_DestroyTexture(texture);
	}
	
	SDL_Surface* surf = TTF_RenderText_Solid(ui->font, text.c_str(), font_color); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

	SDL_Texture* texture = SDL_CreateTextureFromSurface(ui->graphics->renderer, surf); //now you can convert it into a texture
	SDL_FreeSurface(surf);

	width = 0;
	width = 0;
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	bounding_box = Box(0, 0, width, height);

	
	return texture;
}

void SBE::TextSprite::ChangeText(const std::string& text)
{
	this->text = text;
	
	texture = CreateTexture(text);
}

void SBE::TextSprite::ChangeColor(const SDL_Color color)
{
	font_color = color;

	texture = CreateTexture(text);
}
