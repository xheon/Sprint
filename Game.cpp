#include "Game.h"

#include <unordered_map>

#include "Engine.h"
#include "InputSystem.h"
#include "GraphicsSystem.h"

#include "RenderComponent.h"
#include "PlayerInputComponent.h"
#include "AIInputComponent.h"
#include "CheckpointCollisionComponent.h"
#include "TrackCollisionComponent.h"
#include "LapCounterComponent.h"
#include "CarBehaviorComponent.h"
#include "CarCollisionComponent.h"
#include "DebugBoxRenderComponent.h"
#include "DebugLineRenderComponent.h"
#include "DebugCPRenderComponent.h"
#include "PlayerInfoComponent.h"
#include "CountdownComponent.h"
#include "SelectionComponent.h"

#include "TextSprite.h"

#include "Car.h"
#include "Track.h"
#include "WaterPuddle.h"

#include "SDL.h"

using namespace SBE;
using namespace Sprint;

SBE::Game::~Game()
{
	for (auto& car : players)
	{
		delete car;
	}

	for (auto& obj : obstacles)
	{
		delete obj;
	}
}

void SBE::Game::Create()
{
	winner_id = -1;
}

void Game::Init(Engine* engine)
{
	srand(0);
	
	// Initialize the engine
	this->engine = engine;

	// Initialize graphics system
	graphics_system = new GraphicsSystem(engine->renderer);
	engine->AddSystem(graphics_system, true);

	collision_system = new CollisionSystem();
	engine->AddSystem(collision_system);

	ui_system = new UISystem("resources/font.ttf", graphics_system);
	engine->AddSystem(ui_system);



	// Define input mapping for player 1
	std::unordered_map<SDL_Keycode, Key> p1_keys = { {SDLK_LEFT, Key::LEFT},
	{SDLK_RIGHT, Key::RIGHT},
	{SDLK_UP, Key::FORWARD},
	{SDLK_DOWN, Key::BACKWARD} };
	InputMapping* p1_mapping = new InputMapping(p1_keys);

	// Define input mapping for player 2
	std::unordered_map<SDL_Keycode, Key> p2_keys = { { SDLK_a, Key::LEFT },
	{ SDLK_d, Key::RIGHT },
	{ SDLK_w, Key::FORWARD },
	{ SDLK_s, Key::BACKWARD } };
	InputMapping* p2_mapping = new InputMapping(p2_keys);

	// Define input mapping for player 3
	std::unordered_map<SDL_Keycode, Key> p3_keys = { { SDLK_j, Key::LEFT },
	{ SDLK_l, Key::RIGHT },
	{ SDLK_i, Key::FORWARD },
	{ SDLK_k, Key::BACKWARD } };
	InputMapping* p3_mapping = new InputMapping(p3_keys);

	// Setup input system
	input_system = new SBE::InputSystem();
	input_system->AddMapping(p1_mapping);
	input_system->AddMapping(p2_mapping);
	input_system->AddMapping(p3_mapping);
	
	// Let the game listen to the input system
	input_system->AddReveiver(this);
	engine->AddSystem(input_system);


	num_players = 1;
	round = 0;
	CreateCountdown();
	CreateTrack();
	CreatePlayers();
	CreateUI();
	CreateObstacles();
	CreatePlayerSelection();

	winning_screen = new WinningScreen();
	winning_screen->isEnable = false;



	isRunning = true;
	isPaused = false;
}

void SBE::Game::CreateCountdown()
{
	countdown = new Countdown();
	countdown->position = { 800 / 2, 600 / 2 };
	TextSprite* countdown_text = new TextSprite(ui_system, "");
	CountdownComponent* cd_comp = new CountdownComponent();
	cd_comp->Create(countdown, ui_system, countdown_text);

	RenderComponent* cd_render_comp = new RenderComponent();
	cd_render_comp->Create(countdown, graphics_system, countdown_text);

	countdown->AddComponent(cd_comp);
	countdown->AddComponent(cd_render_comp);
	countdown->isEnable = false;
}

void Game::Update(float dt)
{
	if (winner_id > - 1 && isPaused == false)
	{
		SDL_Log("Player %d won", winner_id);
		isPaused = true;
		std::string winner_text = "Player " + std::to_string(winner_id+1) + " won! - Press enter to continue.";
		TextSprite* text = new TextSprite(ui_system, winner_text);
		RenderComponent* comp = new RenderComponent();
		comp->Create(winning_screen, graphics_system, text);
		winning_screen->AddComponent(comp);
		winning_screen->isEnable = true;
		winning_screen->position = { 800 / 2 - comp->GetWidth() / 2, 600 / 2 - comp->GetHeight() / 2 };
	}


	
	if (isPaused == true)
	{
		dt = 0;
	}


	engine->Update();

	track.Update(dt);

	for (auto& obj : obstacles)
	{
		obj->Update(dt);
	}

	for (auto& car : players)
	{
		car->Update(dt);
	}

	selection->Update(dt);
	winning_screen->Update(dt);
	countdown->Update(dt);

	ui->Update(dt);

	engine->UpdateDeferred();	
}

void Game::Receive(Message msg)
{
	switch (msg.GetType())
	{
	case MessageType::GAME_CLOSE:
		isRunning = false;
		break;

	case MessageType::GAME_PAUSE:
		isPaused = !isPaused;
		break;

	case MessageType::PLAYER_FINISHED:
	{
		//Notify(Message{ MessageType::PLAYER_FINISHED, msg.GetInt() });
		winner_id = msg.GetInt();
		round = (round + 1) % 2;
		break;
	}

	case MessageType::DEBUG_BOX_ENABLE:
	{
		for (auto& car : players)
		{
			car->Receive(Message{ MessageType::DEBUG_BOX_ENABLE });
		}
		break;
	}

	case MessageType::DEBUG_AI_ENABLE:
	{
		for (auto& car : players)
		{
			car->Receive(Message{ MessageType::DEBUG_AI_ENABLE });
		}
		break;
	}

	case MessageType::DEBUG_CP_ENABLE:
	{
		track.Receive(Message{ MessageType::DEBUG_CP_ENABLE });
		break;
	}


	case MessageType::GAME_RESTART:
	{
		if (winner_id > -1) // Game was restart after player win
		{
			Reset();
		}
		break;
	}

	case MessageType::PLAYER_SELECT:
	{
		collision_system->ResetDynamics();
		
		num_players = msg.GetInt();
		selection->isEnable = false;
		countdown->isEnable = true;
		players.clear();
		CreatePlayers();
		delete ui;
		CreateUI();

		SDL_Log("Num players: %d", num_players);
		break;
	}

	default:
		break;
	}
}

bool Game::IsRunning()
{
	return isRunning;
}

void SBE::Game::Reset()
{
	CreateTrack();
	for (int i = 0; i < max_players; ++i)
	{
		auto car = players[i];
		car->position = track.GetStartPosition(i + 1) - Vector{ 12,6 };
		car->Reset();
		winner_id = -1;
		isPaused = false;
		winning_screen->isEnable = false;
	}

	countdown->Reset();
	ui->Reset();
	winning_screen->Reset();
}

void SBE::Game::CreateTrack()
{
	std::string path = "resources/";
	std::vector<Checkpoint> cps;
	if (round == 0)
	{
		path += "track_2";
		cps.emplace_back(Vector{ 406,379 }, Vector{ 406,475 });
		cps.emplace_back(Vector{ 635,371 }, Vector{ 717,453 });
		cps.emplace_back(Vector{ 634,249 }, Vector{ 750,249 });
		cps.emplace_back(Vector{ 633,148 }, Vector{ 739, 88 });
		cps.emplace_back(Vector{ 400,145 }, Vector{ 400, 31 });
		cps.emplace_back(Vector{  84, 59 }, Vector{ 173,149 });
		cps.emplace_back(Vector{  53,251 }, Vector{ 172,251 } );
		cps.emplace_back(Vector{  66,431 }, Vector{ 172,371 } );
		cps.emplace_back(Vector{ 382,379 }, Vector{ 382,475 });
	}
	else if (round == 1)
	{
		path += "track_3";
		cps.emplace_back(Vector{ 532, 467 }, Vector{ 532, 559 });
		cps.emplace_back(Vector{ 647, 451 }, Vector{ 719, 525 });
		cps.emplace_back(Vector{ 670, 364 }, Vector{ 764, 364 });
		cps.emplace_back(Vector{ 659, 183 }, Vector{ 770, 167 });
		cps.emplace_back(Vector{ 654, 150 }, Vector{ 730,  79 });
		cps.emplace_back(Vector{ 615, 132 }, Vector{ 615,  30 });
		cps.emplace_back(Vector{ 583, 152 }, Vector{ 503,  81 });
		cps.emplace_back(Vector{ 596, 198 }, Vector{ 467, 166 });
		cps.emplace_back(Vector{ 537, 284 }, Vector{ 459, 217 });
		cps.emplace_back(Vector{ 406, 341 }, Vector{ 406, 239 });
		cps.emplace_back(Vector{ 227, 338 }, Vector{ 227, 247 });
		cps.emplace_back(Vector{ 74, 311 }, Vector{ 133, 228 });
		cps.emplace_back(Vector{ 19, 193 }, Vector{ 119, 194 });
		cps.emplace_back(Vector{ 64,  86 }, Vector{ 135, 162 });
		cps.emplace_back(Vector{ 185,  41 }, Vector{ 186, 144 });
		cps.emplace_back(Vector{ 327, 104 }, Vector{ 243, 163 });
		cps.emplace_back(Vector{ 360, 217 }, Vector{ 258, 216 });
		cps.emplace_back(Vector{ 357, 364 }, Vector{ 257, 363 });
		cps.emplace_back(Vector{ 379, 451 }, Vector{ 300, 519 });
		cps.emplace_back(Vector{ 500, 468 }, Vector{ 500, 558 });
	}
	
	
	// Define track
	RenderComponent* track_renderer = new RenderComponent();
	track_renderer->Create(&track, graphics_system, path +".bmp", false);

	DebugCPRenderComponent* cp_comp = new DebugCPRenderComponent();
	cp_comp->Create(&track, graphics_system, &track.checkpoint_info);
	
	track.Create( path + "_info.bmp", graphics_system, collision_system, cps);
	track.Init();
	track.collision_info.SetOriginalDimensions(track_renderer->GetWidth(), track_renderer->GetHeight());
	track.AddComponent(track_renderer);
	track.AddComponent(cp_comp);
}

void SBE::Game::CreatePlayers()
{
	std::vector<std::string> car_resources;
	car_resources.emplace_back("resources/car_red.bmp");
	car_resources.emplace_back("resources/car_blue.bmp");
	car_resources.emplace_back("resources/car_green.bmp");
	car_resources.emplace_back("resources/car_yellow.bmp");

	//num_players = 1;
	max_players = 4;

	for (int i = 0; i < max_players; ++i)
	{
		Car* car = new Car();
		car->Create(i);
		car->Init();
		car->AddReveiver(this);
		

		// Render component gets the size of the object
		RenderComponent* render_comp = new RenderComponent();
		render_comp->Create(car, graphics_system, car_resources[i]);

		DebugBoxRenderComponent* debug_comp = new DebugBoxRenderComponent();
		debug_comp->Create(car, graphics_system, &render_comp->sprite->bounding_box);

		Vector offset = { render_comp->GetWidth() / 2, render_comp->GetHeight() / 2 };
		car->position = track.GetStartPosition(i + 1) - offset;

		CarBehaviorComponent* behavior_comp = new CarBehaviorComponent();
		behavior_comp->Create(car);

		// Switch between player and AI
		if (i < num_players)
		{
			PlayerInputComponent* input_comp = new PlayerInputComponent();
			input_comp->Create(car, input_system->GetMapping(i));
			input_comp->isEnabled = false;
			car->AddComponent(input_comp);
			countdown->AddReveiver(input_comp);
		}
		else
		{
			AIInputComponent* input_comp = new AIInputComponent();
			input_comp->Create(car, graphics_system, &track.checkpoint_info);
			input_comp->velocity = behavior_comp->GetVelocity();
			input_comp->isEnabled = false;
			car->AddComponent(input_comp);
			countdown->AddReveiver(input_comp);

			// Add line debug renderer to show target
			DebugLineRenderComponent* target_comp = new DebugLineRenderComponent();
			target_comp->Create(car, graphics_system, &input_comp->center, &input_comp->current_target);
			car->AddComponent(target_comp);
		}

		CheckpointCollisionComponent* checkpoints_comp = new CheckpointCollisionComponent();
		checkpoints_comp->Create(car, &track.checkpoint_info, &render_comp->sprite->bounding_box);

		TrackCollisionComponent* track_comp = new TrackCollisionComponent();
		track_comp->Create(car, &track.collision_info);

		LapCounterComponent* lap_comp = new LapCounterComponent();
		lap_comp->Create(car, 3);

		CarCollisionComponent* coll_comp = new CarCollisionComponent();
		coll_comp->Create(car, render_comp->GetWidth(), render_comp->GetHeight());
		// Add bounding box to collision system
		collision_system->AddDynamicObject(car, &coll_comp->bounding_box);


		car->AddComponent(render_comp);
		car->AddComponent(debug_comp);
		car->AddComponent(checkpoints_comp);
		car->AddComponent(track_comp);
		car->AddComponent(lap_comp);
		car->AddComponent(behavior_comp);
		car->AddComponent(coll_comp);

		players.push_back(car);
	}
}

void SBE::Game::CreateObstacles()
{
	WaterPuddle* water = new WaterPuddle();
	water->position = { 689, 275 };

	// Define component
	RenderComponent* render_comp = new RenderComponent;
	render_comp->Create(water, graphics_system, "resources/water.bmp");

	water->AddComponent(render_comp);

	collision_system->AddStaticObject(water, &render_comp->sprite->bounding_box);

	obstacles.push_back(water);
}

void SBE::Game::CreateUI()
{
	ui = new GameInfoUI();
	ui->position = { 0,0 };

	// Define component
	RenderComponent* render_comp = new RenderComponent();
	render_comp->Create(ui, graphics_system, "resources/background.bmp", false);
	ui->AddComponent(render_comp);

	// Add player infos
	Vector offset = { 150, 4 };
	for (auto& p : players)
	{
		PlayerInfoComponent* p_comp = new PlayerInfoComponent();
		p_comp->Create(ui, ui_system, p, &track.checkpoint_info, offset);
		p->AddReveiver(p_comp);
		ui->AddComponent(p_comp);
		offset.x += 150;
	}
		
}

void SBE::Game::CreatePlayerSelection()
{
	std::vector<std::string> controls = { "resources/controls_1.bmp", 
		"resources/controls_2.bmp", "resources/controls_3.bmp" };
	
	selection = new PlayerSelection();
	selection->AddReveiver(this);

	SelectionComponent* select_comp = new SelectionComponent();
	select_comp->Create(selection);
	select_comp->mappings = &input_system->GetMappings();

	selection->AddComponent(select_comp);

	TextSprite* option1 = new TextSprite(ui_system, "One player");
	TextSprite* option2 = new TextSprite(ui_system, "Two players");
	TextSprite* option3 = new TextSprite(ui_system, "Three players");
	select_comp->options.emplace_back(option1, 1);
	select_comp->options.emplace_back(option2, 2);
	select_comp->options.emplace_back(option3, 3);
	select_comp->UpdateSelection();

	float width = 0;
	for (auto& opt : select_comp->options)
	{
		width += opt.first->GetWidth() + 20;
	}
	width /= 2;

	Vector offset = { 800/2 - width - 20, 600/2 - 50.0f };
	int index = 0;
	for (auto& opt : select_comp->options)
	{
		// Add text
		RenderComponent* render_comp = new RenderComponent();
		render_comp->Create(selection, graphics_system, opt.first);
		render_comp->offset = offset;
		selection->AddComponent(render_comp);

		// Add controls image
		RenderComponent* controls_comp = new RenderComponent();
		controls_comp->Create(selection, graphics_system, controls[index]);
		controls_comp->offset = offset;
		controls_comp->offset.y -= 50;
		selection->AddComponent(controls_comp);
		
		offset.x += 150;
		index++;
	}

	RenderComponent* confirm_comp = new RenderComponent();
	TextSprite* text = new TextSprite(ui_system, "Press Enter to Confirm");
	confirm_comp->Create(selection, graphics_system, text);
	confirm_comp->offset = { 800 / 2 - text->GetWidth() / 2, 600 / 2 };
	selection->AddComponent(confirm_comp);

	input_system->AddReveiver(selection);
}
