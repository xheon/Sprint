#ifndef MATH_H
#define MATH_H

#include <vector>

#include "Vector.h"

namespace SBE
{

	class Math
	{
	public:
		static float RadToDeg(float rad);
		static float DegToRad(float deg);
		static float ClampRotation(float orientation);
		static Vector GetMidpoint(const Vector& a, const Vector& b);

		static bool Overlap(const std::pair<float, float>& i1, const std::pair<float, float>& i2);
		static float GetOverlap(const std::pair<float, float>& i1, const std::pair<float, float>& i2);
	};
}

#endif // !MATH_H


