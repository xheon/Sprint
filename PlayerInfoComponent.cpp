#include "PlayerInfoComponent.h"

PlayerInfoComponent::~PlayerInfoComponent()
{
}

void PlayerInfoComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
	lap = 0;
}

void PlayerInfoComponent::Create(SBE::GameObject * parent, SBE::UISystem * ui, Sprint::Car * car, Sprint::CheckpointCollision * cps, Vector offset)
{
	this->parent = parent;
	this->ui = ui;
	this->car = car;
	this->cps = cps;
	lap = 0;

	player_color = GetPlayerColor(car->player_id);

	std::string name = "P" + std::to_string(car->player_id+1);
	playername = new SBE::TextSprite(ui, name, player_color);

	UpdateLapCount();

	this->offset = offset;
}

void PlayerInfoComponent::Update(float dt)
{
	Vector pos = parent->position + offset;

	playername->Draw(pos.x, pos.y);
	lapcount->Draw(pos.x + 30, pos.y);
}

void PlayerInfoComponent::Receive(Message msg)
{
	switch (msg.GetType())
	{
	case MessageType::LAP_FINISHED:
		lap++;
		UpdateLapCount();
		break;

	default:
		break;
	}
}

void PlayerInfoComponent::Reset()
{
	lap = 0;
	UpdateLapCount();
}

void PlayerInfoComponent::UpdateLapCount()
{
	std::string laps = "Lap:   " + std::to_string(lap+1);
	lapcount = new SBE::TextSprite(ui, laps, player_color);
}

SDL_Color PlayerInfoComponent::GetPlayerColor(int index)
{
	switch (index)
	{
	case 0:
		return SDL_Color{ 0xC0, 0x2B, 0x0E };
	case 1:
		return SDL_Color{ 0x54, 0x2B, 0xC8 };
	case 2:
		return SDL_Color{ 0x31, 0xCA, 0x69 };
	case 3:
		return SDL_Color{ 0xFF, 0xFF, 0x00 };
	
	default:
		break;
	}
}