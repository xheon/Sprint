#include "InputSystem.h"

#include "SDL.h"


using namespace SBE;


InputSystem::~InputSystem()
{
	SDL_Log("Shutting down Input system\n");
}

void InputSystem::Update()
{
	// Poll event and set flags
	SDL_Event event;

	while(SDL_PollEvent(&event))
	{
		// Window: Check if window should be closed
		if (event.type == SDL_QUIT)
		{
			window[Window::CLOSE] = true;
			Notify(Message(MessageType::GAME_CLOSE));
		}

		// Register key down
		if (event.type == SDL_KEYDOWN)
		{
			SDL_Keycode code = event.key.keysym.sym;

			// Universal input
			switch (code)
			{
			case SDLK_ESCAPE:  // Close the window on ESC
							   //case SDLK_q:
				Notify(Message(MessageType::GAME_CLOSE));
				window[Window::CLOSE] = true;
				break;

			case SDLK_SPACE:
				Notify(Message(MessageType::GAME_PAUSE));
				break;

			case SDLK_F1:
				Notify(Message(MessageType::DEBUG_BOX_ENABLE));
				break;

			case SDLK_F2:
				Notify(Message(MessageType::DEBUG_CP_ENABLE));
				break;

			case SDLK_F3:
				Notify(Message(MessageType::DEBUG_AI_ENABLE));
				break;

			case SDLK_RETURN:
				Notify(Message(MessageType::GAME_RESTART));
				break;
			/*case SDLK_r:
				keyboard[Key::RESET] = true;
				break;*/

			default:
				break;
			}

			// Mapped input
			for (auto& mapping : mappings)
			{
				// Check if an action is mapped for the pressed 
				if (mapping->IsMapped(code))
				{
					// Get the action associated with the pressed 
					// Set flag for the associated 
					mapping->SetStatus(code, true);
				}
			}
		}


		// Register key up
		if (event.type == SDL_KEYUP)
		{
			SDL_Keycode code = event.key.keysym.sym;

			/*switch (code)
			{
			case SDLK_r:
				keyboard[Key::RESET] = false;
				break;

			default:
				break;
			}*/

			// Mapped input
			for (auto& mapping : mappings)
			{
				// Check if an action is mapped for the pressed 
				if (mapping->IsMapped(code))
				{
					// Get the action associated with the pressed 
					// Set flag for the associated 
					mapping->SetStatus(code, false);
				}
			}
		}
	}
}

void SBE::InputSystem::Receive(Message msg)
{
}

void SBE::InputSystem::AddMapping(InputMapping* mapping)
{
	mappings.push_back(mapping);
}

InputMapping* SBE::InputSystem::GetMapping(int index)
{
	if (index < mappings.size())
	{
		return mappings[index];
	}

	return nullptr;
}

int SBE::InputSystem::GetMappingsCount() const
{
	return mappings.size();
}

std::vector<InputMapping*>& SBE::InputSystem::GetMappings()
{
	return mappings;
}

bool InputSystem::GetStatus(Window action)
{
	// Retunr the status of a specific action performed to the window
	auto a = window.find(action);

	if (a != window.end())
	{
		return a->second;
	}

	return false;
}