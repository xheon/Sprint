#include "SelectionComponent.h"

#include "GameObject.h"


SelectionComponent::~SelectionComponent()
{
}

void SelectionComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
	next = false;
	previous = false;
	frequency = 0.0f;
	selection = 0;
}

void SelectionComponent::Update(float dt)
{
	
	if (options.size() < 1) return;
	
	if (frequency <= 0.0f)
	{
		UpdateAction();

		if (next && selection < options.size() - 1)
		{
			selection = (selection + 1) % options.size();
			next = false;
			SDL_Log("Next %d", selection);
			UpdateSelection();
			frequency = 0.3f;
		}
		else if (previous && selection > 0)
		{
			selection = (selection - 1 + options.size()) % options.size();
			previous = false;
			SDL_Log("Previous %d", selection);
			UpdateSelection();
			frequency = 0.3f;
		}

	}
	else
	{
		frequency -= dt / 1000.0f;
	}
}

void SelectionComponent::UpdateSelection()
{
	for (int i = 0; i < options.size(); ++i)
	{
		SDL_Color color;
		if (selection == i)
		{
			color = { 0xff, 0, 0, 0xff };
		}
		else
		{
			color = { 0xff, 0xff, 0xff, 0xff };
		}

		options[i].first->ChangeColor(color);
	}
}

void SelectionComponent::UpdateAction()
{
	bool input_next = false;
	bool input_previous = false;

	for (auto& mapping : *mappings)
	{
		bool b = mapping->GetStatus(SBE::Key::RIGHT);

		if (b == true)
		{
			input_next = true;
		}

		b = mapping->GetStatus(SBE::Key::LEFT);

		if (b == true)
		{
			input_previous = true;
		}
	}

	if (input_next != input_previous)
	{
		next = input_next;
		previous = input_previous;
	}
}

void SelectionComponent::Receive(Message msg)
{

	switch (msg.GetType())
	{
	case MessageType::SELECTION_NEXT:
		SDL_Log("Next");
		break;

	case MessageType::SELECTION_PREVIOUS:
		SDL_Log("Previous");
		break;

	case MessageType::GAME_RESTART:
		parent->Notify(Message{ MessageType::PLAYER_SELECT, options[selection].second });
		break;

	default:
		break;
	}
}
