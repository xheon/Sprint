#include "RenderComponent.h"

#include "GameObject.h"

using namespace Sprint;

RenderComponent::~RenderComponent()
{
}

void RenderComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void RenderComponent::Create(SBE::GameObject* parent, SBE::GraphicsSystem* graphics, std::string sprite_name)
{
	this->parent = parent;
	this->graphics = graphics;

	sprite = new SBE::Sprite(graphics, sprite_name.c_str());
}

void RenderComponent::Create(SBE::GameObject* parent, SBE::GraphicsSystem* graphics, std::string sprite_name, bool alpha)
{
	this->parent = parent;
	this->graphics = graphics;

	sprite = new SBE::Sprite(graphics, sprite_name.c_str(), alpha);
}

void Sprint::RenderComponent::Create(SBE::GameObject * parent, SBE::GraphicsSystem * graphics, SBE::Sprite * sprite)
{
	this->parent = parent;
	this->graphics = graphics;
	this->sprite = sprite;
}


void RenderComponent::Update(float dt)
{
	sprite->bounding_box.MoveTo(parent->position);
	sprite->bounding_box.RotateTo(parent->orientation);

	sprite->Draw(static_cast<int>(parent->position.x + offset.x), 
		static_cast<int>(parent->position.y + offset.y), parent->orientation);
}

int Sprint::RenderComponent::GetWidth()
{
	return sprite->GetWidth();
}

int Sprint::RenderComponent::GetHeight()
{
	return sprite->GetHeight();
}
