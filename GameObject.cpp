#include "GameObject.h"

#include "Component.h"

using namespace SBE;

GameObject::GameObject() : isEnable(true)
{

}

void SBE::GameObject::Create()
{
	position = { 0,0 };
	orientation = 0;
	isEnable = true;
}

void SBE::GameObject::Update(float dt)
{
	if (isEnable)
	{
		for (const auto& component : components)
		{
			if (component->isEnabled)
			{
				component->Update(dt);
			}
		}
	}
}

void SBE::GameObject::Receive(Message msg)
{
	for (auto& comp : components)
	{
		comp->Receive(msg);
	}
}

void SBE::GameObject::AddComponent(Component * component)
{
	components.push_back(component);
}
