READ ME - SPRINT CLONE by Manuel Dahnert
Course: Game Engine Architecture 2017

1) Controls:

	First player (red):
	- Forward: 		arrow up
	- Backward: 	arrow down
	- Left: arrow 	left
	- Right: arrow 	right

	Second player (blue):
	- Forward: 		w
	- Backward: 	s
	- Left: arrow 	a
	- Right: arrow 	d

	Third player (green):
	- Forward: 		i
	- Backward: 	k
	- Left: arrow 	j
	- Right: arrow 	l


2) Additional input
	
	SPACE: Pause game
	F1: Toggle debug bounding boxes
	F2: Toggle debug checkpoints
	F3: Toggle debug AI target line
	ESC: Quit game
	ENTER: Confirm selection / Start next track


3) Acknowledgments

	- Track tiles by TRBRY (http://opengameart.org/content/race-track-tiles)
	- Track decoration by Casper Nilsson et. al (http://opengameart.org/content/lpc-tile-atlas)
	- Track background by athile (http://opengameart.org/content/seamless-grass-texture-ii)
	- Car assets taken from Super Sprint (http://www.8bbit.com/play/super-sprint/861)
	- SAT implementation following this structure (http://www.dyn4j.org/2010/01/sat/)
	- Line-Line test implementation taken from (http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/)