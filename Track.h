#ifndef TRACK_H
#define TRACK_H

#include "GameObject.h"

#include <vector>

#include "CollisionSystem.h"

#include "TrackCollision.h"
#include "CheckpointCollision.h"
#include "Math.h"

namespace Sprint
{

	class Track : public SBE::GameObject
	{
	public:
		void Create(std::string collision_file, SBE::GraphicsSystem* graphics,
			SBE::CollisionSystem* collisions, std::vector<Checkpoint> cps);

		void Init();
		//void Update(float dt) override;
		void Receive(Message msg) override;

		Vector GetStartPosition(int index);

		TrackCollision collision_info;
		CheckpointCollision checkpoint_info;

		void Reset();

	private:

	};

}

#endif /* TRACK_H */