#include "CarBehaviorComponent.h"

#include "SDL.h"

#include "GameObject.h"
#include "Math.h"

using namespace Sprint;


CarBehaviorComponent::~CarBehaviorComponent()
{
}

void CarBehaviorComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;

	car_accelerate = false;
	car_break = false;
	car_steer_left = false;
	car_steer_right = false;
	wall_hit = false;

	velocity = { 0.0f, 0.0f };
	turn_rate = 300.0f;
	acceleration_rate = 80.0f;
	drag_rate = 70.0f;
	max_speed = 200.0f;
}

void CarBehaviorComponent::Update(float dt)
{

	if (car_steer_left)
	{
		Steer(dt, false);
		//SDL_Log("Left");

	}
	else if (car_steer_right)
	{
		Steer(dt, true);
		//SDL_Log("Right");
	}

	if (car_accelerate)
	{
		Accelerate(dt);
		//SDL_Log("Accelerate");
	}
	else if (car_break)
	{
		Break(dt);
		//SDL_Log("Break");
	}
	else
	{
		Drag(dt);
	}

	if (wall_hit)
	{
		Wall_Hit(dt);
		SDL_Log("Wall hit");
	}

	if (car_hit)
	{
		Car_Hit(dt);
		SDL_Log("Car hit");
	}

	if (velocity.Length() > 0.1)
	{
		parent->position.x = parent->position.x + (dt / 1000.0f) * velocity.x;
		parent->position.y = parent->position.y + (dt / 1000.0f) * velocity.y;
	}

	if (penalty > 0)
	{
		penalty -= dt / 1000.0f;
	}

}

void Sprint::CarBehaviorComponent::Reset()
{
	velocity = { 0,0 };
	car_accelerate = false;
	car_break = false;
	car_steer_left = false;
	car_steer_right = false;
	wall_hit = false;
	penalty = 0;
}

void CarBehaviorComponent::Receive(Message msg)
{
	switch (msg.GetType())
	{
	case MessageType::ACCELERATE:
		car_accelerate = msg.GetBool();
		break;

	case MessageType::BREAK:
		car_break = msg.GetBool();
		break;

	case MessageType::STEER_LEFT:
		car_steer_left = msg.GetBool();
		break;

	case MessageType::STEER_RIGHT:
		car_steer_right = msg.GetBool();
		break;

	case MessageType::WALL_HIT:
		wall_hit = msg.GetBool();
		break;

	case MessageType::CAR_RESPONSE:
		car_hit = msg.GetBool();
		break;

	case MessageType::OBSTACLE_HIT:
	{
		if (penalty <= 0.0f)
		{
			velocity = velocity * 0.35f;
			penalty = 0.5f;
		}

		break;
	}

	default:
		break;
	}
}

void Sprint::CarBehaviorComponent::Accelerate(float dt)
{
	if (velocity.Length() < max_speed)
	{
		auto car_dir = Vector::ForwardVector(parent->orientation);
		velocity = velocity + car_dir * (dt / 1000.0f) * acceleration_rate;
	}
}

void Sprint::CarBehaviorComponent::Break(float dt)
{
	if (velocity.Length() < max_speed)
	{
		auto car_dir = Vector::ForwardVector(parent->orientation);
		velocity = velocity - car_dir * (dt / 1000.0f) * acceleration_rate;
	}
}

void Sprint::CarBehaviorComponent::Drag(float dt)
{
	
	if (velocity.Length() > 0.0f)
	{
		auto car_dir = Vector::ForwardVector(parent->orientation); // Get car direction
		
		// Apply drag in opposite car direction
		velocity = velocity - car_dir * GetDirection() * (dt / 1000.0) * drag_rate;
	}

	if (velocity.Length() < 10)
	{
		velocity = { 0, 0 };
	}
}

void Sprint::CarBehaviorComponent::Steer(float dt, bool clockwise)
{
	int direction = 1;

	if (clockwise == true)
	{
		direction = 1;
	}
	else
	{
		direction = -1;
	}

	float orientation_old = parent->orientation;
	parent->orientation = (parent->orientation + direction * (dt / 1000.0f) * turn_rate);
	parent->orientation = SBE::Math::ClampRotation(parent->orientation);
	float delta = parent->orientation - orientation_old;
	velocity.Rotate(delta);
}

int Sprint::CarBehaviorComponent::GetDirection() const
{
	// Check if I'm driving forward or backward
	// Do this by comparing the velocity direction and the car direction
	auto vel_dir = velocity.Normalize();
	auto car_dir = Vector::ForwardVector(parent->orientation); // Get opposite car direction
	auto opposite = car_dir * (-1); // Flip the vector

	int direction = 1;

	// Compare velocity direction opposite car direction with a certain threshold
	if (abs(vel_dir.x - opposite.x) < 0.5f && abs(vel_dir.y - opposite.y) < 0.5f)
	{
		direction = -1;
	}

	return direction;

}

void Sprint::CarBehaviorComponent::Wall_Hit(float dt)
{
	Vector reflection = Vector::ForwardVector(parent->orientation);
	velocity = reflection * velocity.Length() * 0.5f;

	wall_hit = false;
}

void Sprint::CarBehaviorComponent::Car_Hit(float dt)
{
	Vector reflection = Vector::ForwardVector(parent->orientation);
	velocity = reflection * velocity.Length() * 0.5f;

	car_hit = false;
}

Vector * Sprint::CarBehaviorComponent::GetVelocity()
{
	return &velocity;
}
