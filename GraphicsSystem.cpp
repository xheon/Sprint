#include "GraphicsSystem.h"

#include "SDL.h"

#include "Box.h"

using namespace SBE;

GraphicsSystem::GraphicsSystem(SDL_Renderer * renderer) :
	renderer(renderer)
{
}

GraphicsSystem::~GraphicsSystem()
{
	SDL_Log("Shutting down Graphics system\n");
}

void GraphicsSystem::Update()
{
	SDL_RenderPresent(renderer);
	SDL_RenderClear(renderer);
}

void GraphicsSystem::Receive(Message msg)
{
}

void GraphicsSystem::DrawLine(const Vector & p1, const Vector & p2)
{
	// Draw line in current color
	SDL_RenderDrawLine(renderer, static_cast<int>(p1.x), static_cast<int>(p1.y),
		static_cast<int>(p2.x), static_cast<int>(p2.y));
}

void GraphicsSystem::DrawLine(const Vector & p1, const Vector & p2, const SDL_Color& color)
{
	// Save current drawing color
	SDL_Color old;
	SDL_GetRenderDrawColor(renderer, &old.r, &old.g, &old.b, &old.a);

	// Set new drawing color
	SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
	
	// Draw the actual line
	DrawLine(p1, p2);

	// Restore drawing color
	SDL_SetRenderDrawColor(renderer, old.r, old.g, old.b, old.a);
}

void SBE::GraphicsSystem::DrawRect(const Vector& p1, const Vector& p2, const Vector& p3, const Vector& p4)
{
	DrawLine(p1, p2); // Top line
	DrawLine(p2, p4); // Right line
	DrawLine(p3, p4); // Bottom line
	DrawLine(p1, p3); // Left line
}

void SBE::GraphicsSystem::DrawRect(Box & box)
{
	DrawLine(box.GetPoint(0), box.GetPoint(1), { 0x00, 0xFF, 0xFF, 0xFF }); // Top line
	DrawLine(box.GetPoint(1), box.GetPoint(3), { 0x00, 0xFF, 0xFF, 0xFF }); // Right line
	DrawLine(box.GetPoint(2), box.GetPoint(3), { 0x00, 0xFF, 0xFF, 0xFF }); // Bottom line
	DrawLine(box.GetPoint(0), box.GetPoint(2), { 0x00, 0xFF, 0xFF, 0xFF }); // Left line
}
