#pragma once

#include "Component.h"
#include "Observer.h"

#include "TextSprite.h"
#include "UISystem.h"

#include "Car.h"
#include "CheckpointCollision.h"

class PlayerInfoComponent : public SBE::Component, public SBE::Observer
{
public:
	~PlayerInfoComponent() override;

	// Inherited via Component
	virtual void Create(SBE::GameObject * parent) override;
	void Create(SBE::GameObject* parent, SBE::UISystem* ui, Sprint::Car* car, Sprint::CheckpointCollision* cps, Vector offset);
	virtual void Update(float dt) override;
	virtual void Receive(Message msg) override;
	void Reset() override;

	void UpdateLapCount();

	SDL_Color GetPlayerColor(int index);

private:
	SBE::UISystem* ui;
	Sprint::Car* car;
	Sprint::CheckpointCollision* cps;
	Vector offset;

	SDL_Color player_color;
	
	SBE::TextSprite* playername;
	SBE::TextSprite* lapcount;
	int lap;

	// Inherited via Observer
};

