#ifndef CHECKPOINT_H
#define CHECKPOINT_H

#include "Vector.h"

class Checkpoint
{
public:
	Checkpoint(Vector a, Vector b);
	
	Vector p1;
	Vector p2;
};

#endif /* CHECKPOINT_H */