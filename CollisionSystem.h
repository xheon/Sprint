#ifndef COLLISIONSYSTEM_H
#define COLLISIONSYSTEM_H

#include "System.h"

#include <vector>

#include "Vector.h"
#include "Box.h"
#include "GameObject.h"

namespace SBE
{

	class CollisionSystem : public System
	{
	public:

		bool LineLineIntersection(const Vector& a1, const Vector& a2,
			const Vector& b1, const Vector& b2);

		void SetupCarCollision(std::vector<std::pair<GameObject*, Box>>& collision_info);

		void CheckDynamicCollsions();
		void CheckStaticCollsions();

		bool ObbObbIntersection(const Box& b1, const Box& b2, std::pair<float, Vector>& collision_info);

		void AddDynamicObject(GameObject* obj, Box* box);
		void AddStaticObject(GameObject* obj, Box* box);

		void ResetDynamics();

	private:
		int GetOrientation(const Vector& p, const Vector& q, const Vector& r);
		bool OnSegment(const Vector& p, const Vector& q, const Vector& r);

		// Inherited via System
		virtual void Update() override;
		virtual void Receive(Message msg) override;


		std::vector<std::pair<GameObject*, Box*>> dynamic_objects;
		std::vector<std::pair<GameObject*, Box*>> static_objects;
	};
}

#endif /* COLLISIONSYSTEM_H */