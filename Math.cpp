#include "Math.h"

#include <algorithm>

#include <math.h>

#include "SDL_stdinc.h"

using namespace SBE;

float Math::DegToRad(float rad)
{
	return rad * M_PI / 180.0f;
}

float Math::RadToDeg(float deg)
{
	return deg * 180.0f / M_PI;
}

float Math::ClampRotation(float orientation)
{
	return fmod(orientation + 360, 360);
}

Vector SBE::Math::GetMidpoint(const Vector & a, const Vector & b)
{
	Vector temp = (a + b) / 2;

	return temp;
}

bool SBE::Math::Overlap(const std::pair<float, float>& i1, const std::pair<float, float>& i2)
{
	// Compute widths of the intervals
	float w1 = i1.second - i1.first;
	float w2 = i2.second - i2.first;
	float sum = w1 + w2;

	// Compute min and max values
	float min = std::min(i1.first, i2.first);
	float max = std::max(i1.second, i2.second);
	float diff = max - min;

	// The width of both intervals is larger than the total length
	if (diff < sum)
	{
		return true;
	}

	return false;
}

float SBE::Math::GetOverlap(const std::pair<float, float>& i1, const std::pair<float, float>& i2)
{
	float w1 = i1.second - i1.first;
	float w2 = i2.second - i2.first;
	float sum = w1 + w2;

	// Compute min and max values
	float min = std::min(i1.first, i2.first);
	float max = std::max(i1.second, i2.second);
	float diff = max - min;

	float overlap = sum - diff;

	return overlap;
}


