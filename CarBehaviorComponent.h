#ifndef CARBEHAVIORCOMPONENT_H
#define CARBEHAVIORCOMPONENT_H

#include "Component.h"

#include "Vector.h"

namespace Sprint
{

	class CarBehaviorComponent : public SBE::Component
	{
	public:
		~CarBehaviorComponent();

		// Inherited via Component
		virtual void Create(SBE::GameObject * parent) override;
		virtual void Update(float dt) override;
		void Reset() override;

		void Receive(Message msg) override;

		void Accelerate(float dt);
		void Break(float dt);
		void Drag(float dt);		
		void Steer(float dt, bool clockwise);

		int GetDirection() const;

		void Wall_Hit(float dt);
		void Car_Hit(float dt);

		Vector* GetVelocity();

	private:		
		// Car state
		bool car_accelerate;
		bool car_break;
		bool car_steer_left;
		bool car_steer_right;

		bool wall_hit;
		bool car_hit;

		Vector velocity;

		// Car properties
		float max_speed;
		float turn_rate;
		float drag_rate;
		float acceleration_rate;

		float penalty;
	};
}


#endif // !CARBEHAVIORCOMPONENT_H
