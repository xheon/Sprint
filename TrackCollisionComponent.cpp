#include "TrackCollisionComponent.h"

#include "Math.h"

#include "GameObject.h"

using namespace Sprint;


TrackCollisionComponent::~TrackCollisionComponent()
{
}

void TrackCollisionComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
}

void TrackCollisionComponent::Create(SBE::GameObject * parent, TrackCollision* collision)
{
	this->parent = parent;
	this->collision = collision;
}

void TrackCollisionComponent::Update(float dt)
{
	if (!OnRoad(parent->position.x, parent->position.y))
	{
		float orientation = parent->orientation;
		Vector track_normal = collision->GetTrackNormal(parent->position);

		Vector direction = Vector::ForwardVector(orientation);

		Vector reflection = direction.Reflect(track_normal);

		float degree = reflection.Orientation();
		parent->orientation = SBE::Math::ClampRotation(degree);

		// Reset position
		parent->position = position_old;
		parent->Receive(Message(MessageType::WALL_HIT));
	}

	position_old = parent->position;
}


bool TrackCollisionComponent::OnRoad(float x, float y)
{
	return collision->OnRoad(x, y);
}


