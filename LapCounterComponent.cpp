#include "LapCounterComponent.h"

#include "SDL.h"

#include "GameObject.h"

using namespace Sprint;

Sprint::LapCounterComponent::~LapCounterComponent()
{
}

void Sprint::LapCounterComponent::Create(SBE::GameObject * parent)
{
	this->parent = parent;
	this->max_rounds = 3;
	this->lap_counter = 0;
	this->finished = false;
}

void LapCounterComponent::Create(SBE::GameObject * parent, int max_rounds)
{
	Create(parent);
	this->max_rounds = max_rounds;
}

void LapCounterComponent::Update(float dt)
{
	if (lap_counter >= max_rounds && finished == false)
	{
		finished = true;
		
		parent->Receive(Message{ MessageType::PLAYER_FINISHED });

		SDL_Log("Player finished");
	}
}

void LapCounterComponent::Receive(Message msg)
{
	switch (msg.GetType())
	{
	case MessageType::LAP_FINISHED:
		lap_counter++;
		SDL_Log("Lap %d finished ", lap_counter);

		break;

	default:
		break;
	}
}

void Sprint::LapCounterComponent::Reset()
{
	lap_counter = 0;
	finished = false;
}


