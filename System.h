#ifndef SYSTEM_H
#define SYSTEM_H

#include "Subject.h"
#include "Observer.h"

namespace SBE
{

	class System : public Subject, public Observer
	{
	public:
		virtual ~System() {};

		virtual void Update() = 0;
		virtual void Receive(Message msg) override = 0;
	};

}

#endif /* SYSTEM_H */