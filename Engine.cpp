#include "Engine.h"

#include "InputSystem.h"
#include "GraphicsSystem.h"

using namespace SBE;

bool Engine::Init()
{
	SDL_Log("Initializing the engine.\n");

	// Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) 
	{
		SDL_Log("Unable to initialize SDL: %s",	SDL_GetError());

		return false;
	}
	
	// Create the window
	window = SDL_CreateWindow("Sprint",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		800, 600, SDL_WINDOW_SHOWN);

	if (window == nullptr)
	{
		SDL_Log("Unable to initialize the window: %s\n", SDL_GetError());

		return false;
	}

	// Create the renderer
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (renderer == nullptr)
	{
		SDL_Log("Unable to initialize the renderer: %s\n", SDL_GetError());
		
		return false;
	}

	// Initialize systems
	//systems.emplace("input", new InputSystem());
	//systems.emplace("graphics", new GraphicsSystem());

	isRunning = true;

	return true;
}

void Engine::Destroy()
{
	SDL_Log("Shutting down the engine\n");

	SDL_Log("Shutting down the subsystems\n");
	
	/*
	for (auto& system : systems)
	{
		delete system.first;
	}*/

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	SDL_Quit();
}


void Engine::Update()
{
	// Update the subsystems
	for (auto& system : systems)
	{
		if (system.second == false)
		{
			system.first->Update();
		}
	}
}

void Engine::UpdateDeferred()
{
	// Update the subsystems
	for (auto& system : systems)
	{
		if (system.second == true)
		{
			system.first->Update();
		}
	}
}

bool Engine::IsRunning()
{
	return isRunning;
}

void Engine::AddSystem(System * system)
{
	AddSystem(system, false);
}

void Engine::AddSystem(System * system, bool deferred)
{
	systems.emplace_back(system, deferred);
}

int SBE::Engine::GetElapsedTime()
{
	return SDL_GetTicks();
}
