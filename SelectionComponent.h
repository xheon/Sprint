#pragma once

#include "Component.h"
#include "Observer.h"
#include <vector>
#include "TextSprite.h"
#include "InputMapping.h"

class SelectionComponent : public SBE::Component, public SBE::Observer
{
public:
	~SelectionComponent() override;

	// Inherited via Component
	virtual void Create(SBE::GameObject * parent) override;
	virtual void Update(float dt) override;

	void UpdateSelection();

	void UpdateAction();

	// Inherited via Observer
	virtual void Receive(Message msg) override;


	std::vector<SBE::InputMapping*>* mappings;
	std::vector<std::pair<SBE::TextSprite*, int>> options;
	int selection;

	bool next;
	bool previous;

	float frequency;

	
};

