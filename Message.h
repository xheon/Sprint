#ifndef MESSAGE_H
#define MESSAGE_H

#include "Vector.h"

enum class MessageType
{
	// Game state
	GAME_START,
	GAME_CLOSE,
	GAME_PAUSE,
	GAME_RESTART,
	PLAYER_SELECT,

	// Debug
	DEBUG_BOX_ENABLE,
	DEBUG_CP_ENABLE,
	DEBUG_AI_ENABLE,

	// Car behavior
	ACCELERATE,
	BREAK,
	STEER_LEFT,
	STEER_RIGHT,

	// Collision
	WALL_HIT,
	CHECKPOINT_HIT,
	CAR_HIT,
	CAR_RESPONSE,
	OBSTACLE_HIT,

	// Player state
	LAP_FINISHED,
	PLAYER_FINISHED,

	// UI
	SELECTION_NEXT,
	SELECTION_PREVIOUS
};

class Message
{
public:
	Message(MessageType type);
	Message(MessageType type, int value);
	Message(MessageType type, float value, Vector vec_value);
	Message(MessageType type, bool value);

	MessageType GetType();

	int GetInt();
	void SetInt(int value);

	float GetFloat();
	void SetFloat(float value);

	bool GetBool();
	void SetBool(bool value);

	Vector& GetVector();
	void SetVector(Vector value);

private:
	MessageType type;
	
	// Values
	int int_value;
	float float_value;
	bool bool_value;
	Vector vector_value;
	//CollisionInfo collision_info;
};

#endif /* MESSAGE_H */