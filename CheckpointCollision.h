#ifndef CHECKPOINTCOLLISION_H
#define CHECKPOINTCOLLISION_H

#include <vector>

#include "GraphicsSystem.h"
#include "CollisionSystem.h"

#include "Box.h"
#include "Checkpoint.h"

namespace Sprint
{

	class CheckpointCollision
	{
	public:
		void Create(SBE::GraphicsSystem* graphics,
			SBE::CollisionSystem* collisions, std::vector<Checkpoint> cps);

		void DrawCheckpoints();

		//int IsHit(float x, float y);
		int IsHit(const Box& bounding_box);

		int GetCheckpointsCount() const;
		Checkpoint* GetCheckkpoint(int index);

		std::vector<Checkpoint> checkpoints;

	private:
		SBE::GraphicsSystem* graphics;
		SBE::CollisionSystem* collisions;
	};
}


#endif // !CHECKPOINTCOLLISION_H
