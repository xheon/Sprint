#ifndef CARCOLLISIONCOMPONENT_H
#define CARCOLLISIONCOMPONENT_H

#include "Component.h"

#include "Box.h"


namespace Sprint
{
	class CarCollisionComponent : public SBE::Component
	{
	public:
		~CarCollisionComponent();

		// Inherited via Component
		virtual void Create(SBE::GameObject * parent) override;
		void Create(SBE::GameObject * parent, float width, float height);
		virtual void Update(float dt) override;

		void Receive(Message msg) override;
		void CollisionResponse(float overlap, Vector& normal);

		Box bounding_box;

		float penalty;
	};
}


#endif // !CARCOLLISIONCOMPONENT_H
