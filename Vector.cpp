#include "Vector.h"

#include <math.h>

#include "Math.h"

using namespace SBE;

Vector::Vector() :
	x(0), y(0)
{
}

Vector::Vector(float x, float y) :
	x(x), y(y)
{
}

Vector::Vector(double x, double y) :
	x(static_cast<float>(x)), y(static_cast<float>(y))
{
}

Vector::Vector(int x, int y) :
	x(x), y(y)
{
}

float Vector::Length() const
{
	float length = sqrt(pow(x, 2) + pow(y, 2));

	return length;
}

float Vector::Orientation() const
{
	Vector dir = Normalize();
	float rad = atan2(dir.y, dir.x);
	float degree = Math::RadToDeg(rad);
	degree = Math::ClampRotation(degree);

	return degree;
}

float Vector::OrientationAbs() const
{
	Vector dir = Normalize();
	float rad = atan2(dir.y, dir.x);
	float degree = Math::RadToDeg(rad);

	return degree;
}

void Vector::Rotate(float degree)
{
	float angle_rad = Math::DegToRad(degree);
	double sa = sin(angle_rad);
	double ca = cos(angle_rad);

	x = x * ca - y * sa;
	y = x * sa + y * ca;
}

Vector Vector::ForwardVector(float degree)
{
	// Create vector from an orientation
	float rad = Math::DegToRad(degree);
	float c = cos(rad);
	float s = sin(rad);
	
	return Vector(c, s);
}

Vector Vector::RotatePoint(Vector& point, float angle, Vector& center)
{
	float angle_rad = Math::DegToRad(angle);
	double s = sin(angle_rad);
	double c = cos(angle_rad);

	Vector point_rot;

	// Translate point to origin
	point_rot.x = point.x - center.x;
	point_rot.y = point.y - center.y;

	// Rotate point
	float x_new = point_rot.x * c - point_rot.y * s;
	float y_new = point_rot.x * s + point_rot.y * c;

	point_rot.x = x_new + center.x;
	point_rot.y = y_new + center.y;
	
	return point_rot;
}

Vector Vector::HeadingVector(float angle)
{
	float rad = Math::DegToRad(angle);
	Vector direction = Vector{ cos(rad), sin(rad) };

	return direction;
}

Vector Vector::Normalize() const
{
	Vector temp;

	float length = Length(); // Save length
	
	if (length <= 0.0f)
	{
		return Vector{ 0,0 };
	}

	temp.x = x / length;
	temp.y = y / length;

	return temp;
}

Vector Vector::Reflect(const Vector & normal) const
{
	float dot = Dot(normal);
	Vector reflection = *this - normal * 2 * dot;

	return reflection;
}

Vector Vector::Abs() const
{
	return Vector(abs(x), abs(y));
}

float Vector::Dot(Vector const& rhs) const
{
	return x * rhs.x + y * rhs.y;
}

Vector Vector::operator-(Vector const & rhs) const
{
	Vector temp;
	temp.x = x - rhs.x;
	temp.y = y - rhs.y;

	return temp;
}

Vector Vector::operator+(Vector const & rhs) const
{
	Vector temp;
	temp.x = x + rhs.x;
	temp.y = y + rhs.y;

	return temp;
}

Vector Vector::operator*(float const & scalar) const
{
	Vector temp;
	temp.x = x * scalar;
	temp.y = y * scalar;

	return temp;
}


Vector Vector::operator/(float const & scalar) const
{
	Vector temp;
	temp.x = x / scalar;
	temp.y = y / scalar;

	return temp;
}

Vector Vector::operator*(Vector const & rhs) const
{
	Vector temp;
	temp.x = x * rhs.x;
	temp.y = y * rhs.y;

	return temp;
}

Vector Vector::operator/(Vector const & rhs) const
{
	Vector temp;
	temp.x = x / rhs.x;
	temp.y = y / rhs.y;

	return temp;
}
