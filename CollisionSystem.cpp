#include "CollisionSystem.h"

#include <iostream>

#include "SDL.h"

#include "Math.h"

using namespace std;
using namespace SBE;


void SBE::CollisionSystem::Update()
{
	CheckDynamicCollsions();
	CheckStaticCollsions();
}

void SBE::CollisionSystem::Receive(Message msg)
{
}

void SBE::CollisionSystem::AddDynamicObject(GameObject* obj, Box* box)
{
	dynamic_objects.emplace_back(obj, box);
}

void SBE::CollisionSystem::AddStaticObject(GameObject * obj, Box * box)
{
	static_objects.emplace_back(obj, box);
}

void SBE::CollisionSystem::ResetDynamics()
{
	dynamic_objects.clear();
}

bool CollisionSystem::ObbObbIntersection(const Box& b1, const Box& b2, std::pair<float, Vector>& collision_info)
{
	// Following the structure of http://www.dyn4j.org/2010/01/sat/

	// Define axes for both objects
	Vector axes1[2];
	axes1[0] = b1.GetPoint(1) - b1.GetPoint(0);
	axes1[0] = axes1[0].Normalize();
	axes1[1] = b1.GetPoint(2) - b1.GetPoint(0);
	axes1[1] = axes1[1].Normalize();

	Vector axes2[2];
	axes2[0] = b2.GetPoint(1) - b2.GetPoint(0);
	axes2[0] = axes2[0].Normalize();
	axes2[1] = b2.GetPoint(2) - b2.GetPoint(0);
	axes2[1] = axes2[1].Normalize();

	// Save smallest overlap
	float min_overlap = std::numeric_limits<float>::max();
	Vector min_axis;

	// Loop through axes1
	for (auto& axis : axes1)
	{
		auto b1_extend = b1.Project(axis);
		auto b2_extend = b2.Project(axis);

		if (Math::Overlap(b1_extend, b2_extend) == false)
		{
			return false;
		}
		else
		{
			float overlap = Math::GetOverlap(b1_extend, b2_extend);

			if (overlap < min_overlap)
			{
				min_overlap = overlap;
				min_axis = axis;
			}
		}
	}

	// Loop through axes2
	for (auto& axis : axes2)
	{
		auto b1_extend = b1.Project(axis);
		auto b2_extend = b2.Project(axis);

		if (Math::Overlap(b1_extend, b2_extend) == false)
		{
			return false;
		}
		else
		{
			float overlap = Math::GetOverlap(b1_extend, b2_extend);

			if (overlap < min_overlap)
			{
				min_overlap = overlap;
				min_axis = axis;
			}
		}
	}
	

	collision_info.first = min_overlap;
	collision_info.second = min_axis;
	//SDL_Log("Overlap: %f, Axis: %f, %f", min_overlap, min_axis.x, min_axis.y);

	return true;
}

// The main function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.
bool CollisionSystem::LineLineIntersection(const Vector& p1, const Vector& q1,
	const Vector& p2, const Vector& q2)
{
	// Find the four orientations needed for general and
	// special cases
	int o1 = GetOrientation(p1, q1, p2);
	int o2 = GetOrientation(p1, q1, q2);
	int o3 = GetOrientation(p2, q2, p1);
	int o4 = GetOrientation(p2, q2, q1);

	// General case
	if (o1 != o2 && o3 != o4)
		return true;

	// Special Cases
	// p1, q1 and p2 are colinear and p2 lies on segment p1q1
	if (o1 == 0 && OnSegment(p1, p2, q1)) return true;

	// p1, q1 and p2 are colinear and q2 lies on segment p1q1
	if (o2 == 0 && OnSegment(p1, q2, q1)) return true;

	// p2, q2 and p1 are colinear and p1 lies on segment p2q2
	if (o3 == 0 && OnSegment(p2, p1, q2)) return true;

	// p2, q2 and q1 are colinear and q1 lies on segment p2q2
	if (o4 == 0 && OnSegment(p2, q1, q2)) return true;

	return false; // Doesn't fall in any of the above cases
}

void SBE::CollisionSystem::SetupCarCollision(std::vector<std::pair<GameObject*, Box>>& collision_info)
{
	//car_collision = collision_info;
}


// http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
// Given three colinear points p, q, r, the function checks if
// point q lies on line segment 'pr'
bool CollisionSystem::OnSegment(const Vector& p, const Vector& q, const Vector& r)
{
	if (q.x <= fmax(p.x, r.x) && q.x >= fmin(p.x, r.x) &&
		q.y <= fmax(p.y, r.y) && q.y >= fmin(p.y, r.y))
		return true;

	return false;
}


void SBE::CollisionSystem::CheckDynamicCollsions()
{
	if (dynamic_objects.size() < 1) return;

	// Check car collisions
	for (auto& g1 = dynamic_objects.begin(); g1 != dynamic_objects.end() - 1; ++g1)
	{
		for (auto& g2 = g1 + 1; g2 != dynamic_objects.end(); ++g2)
		{
			std::pair<float, Vector> collision_info;
			// Pairwise comparision of g1 and g2
			if (ObbObbIntersection(*g1->second, *g2->second, collision_info))
			{
				if (collision_info.first > 5)
				{
					// Calculate normal
					Vector rh_normal = collision_info.second * (-1);
					Vector lh_normal = collision_info.second;
					float half_length = collision_info.first;

					// Notify both objects
					g1->first->Receive(Message{ MessageType::CAR_HIT, half_length, rh_normal });
					g2->first->Receive(Message{ MessageType::CAR_HIT, half_length, lh_normal });
				}
			}
		}
	}
}

void SBE::CollisionSystem::CheckStaticCollsions()
{
	// Check car collisions
	for (auto& g1 = dynamic_objects.begin(); g1 != dynamic_objects.end(); ++g1)
	{
		for (auto& g2 = static_objects.begin(); g2 != static_objects.end(); ++g2)
		{
			std::pair<float, Vector> collision_info;
			// Pairwise comparision of g1 and g2
			if (ObbObbIntersection(*g1->second, *g2->second, collision_info))
			{
				// Notify dynamic objects
				g1->first->Receive(Message{ MessageType::OBSTACLE_HIT });
				
			}
		}
	}
}

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
int CollisionSystem::GetOrientation(const Vector& p, const Vector& q, const Vector& r)
{
	// See http://www.geeksforgeeks.org/orientation-3-ordered-points/
	// for details of below formula.
	int val = (q.y - p.y) * (r.x - q.x) -
		(q.x - p.x) * (r.y - q.y);

	if (val == 0) return 0;  // colinear

	return (val > 0) ? 1 : 2; // clock or counterclock wise
}

