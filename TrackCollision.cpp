#include "TrackCollision.h"

#include <iostream>

#include "SDL.h"

#include "Vector.h"

using namespace Sprint;

void TrackCollision::Create(std::string image_file)
{
	CreateCollisionGrid(image_file);
}

bool TrackCollision::OnRoad(float x, float y)
{
	// Get coordinates in dimensions of the grid
	int grid_x = floor(x / original_width * width);
	int grid_y = floor(y / original_height * height);

	int index = GetIndex(grid_x, grid_y);

	// Exit if index is outsite of grid
	if (index >= grid.size())
	{
		return false;
	}

	bool collision = grid[index];

	return collision;
}

void TrackCollision::SetOriginalDimensions(int width, int height)
{
	original_width = width;
	original_height = height;
}

float Sprint::TrackCollision::GetWidthRatio()
{
	return static_cast<float>(original_width) / static_cast<float>(width);
}

float Sprint::TrackCollision::GetHeightRatio()
{
	return static_cast<float>(original_height) / static_cast<float>(height);
}


void TrackCollision::CreateCollisionGrid(std::string& file)
{
	SDL_Surface* surface = SDL_LoadBMP(file.c_str());

	// Check if image file is available
	if (surface == nullptr)
	{
		SDL_Log("Unable to load image: %s\n", SDL_GetError());
		return;
	}

	Uint8* pxs = static_cast<Uint8*>(surface->pixels);
	int bpp = surface->format->BytesPerPixel;

	width = surface->w;
	height = surface->h;

	// Resize grid to allow direct access
	grid.resize(width  * height, false);

	// Define road matrix
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			Uint8* p = pxs + y * surface->pitch + x * bpp;

			bool isRoad = false;

			// Check if px is road
			if (*p != 0)
			{
				isRoad = true;
			}

			int index = GetIndex(x, y);
			grid[index] = isRoad;
		}
	}

	SDL_FreeSurface(surface);
}


int TrackCollision::GetIndex(int x, int y)
{
	int index = y * width + x;
	return index;
}

void TrackCollision::PrintCollision()
{
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			int index = GetIndex(x, y);

			if (grid[index]) std::cout << 'x'; // road
			else std::cout << "-"; // terrain
		}
		std::cout << std::endl;
	}
}


Vector TrackCollision::GetTrackNormal(const Vector & position)
{
	// Calculate the normal at a given position

	// Idea
	// loop over each neighbor in the collision grid
	// calculate the relative position of the neighbor
	// add x and y to normal vector if the neighbor is road

	float x_pos = position.x;
	float y_pos = position.y;

	// Resulting normal vector
	Vector normal = { 0,0 };

	// Loop over all neighbors
	for (int i = -1; i < 2; ++i)
	{
		float x = x_pos + i * GetWidthRatio();

		for (int j = -1; j < 2; ++j)
		{
			if (i == 0 && j == 0) continue;

			float y = y_pos + j * GetHeightRatio();

			// Get index of neighbor
			bool isRoad = OnRoad(x, y);

			if (isRoad)
			{
				// Relative position
				float rel_x = x - x_pos;
				float rel_y = y - y_pos;

				// Add to normal vector
				normal.x += rel_x;
				normal.y += rel_y;
			}
		}
	}

	// Normalize vector
	normal = normal.Normalize();

	return normal;
}