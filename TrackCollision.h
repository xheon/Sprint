#ifndef TRACKCOLLISION_H
#define TRACKCOLLISION_H

#include <vector>

#include "Vector.h"

namespace Sprint
{

	class TrackCollision
	{
	public:
		void Create(std::string image_file);

		bool OnRoad(float x, float y);
		void SetOriginalDimensions(int width, int height);

		Vector GetTrackNormal(const Vector& position);

		float GetWidthRatio();
		float GetHeightRatio();

	private:
		int GetIndex(int x, int y);

		void PrintCollision();

		void CreateCollisionGrid(std::string & file);

		std::vector<bool> grid;
		int width;
		int height;

		int original_width;
		int original_height;
	};
}

#endif // !TRACKCOLLISION_H


