#include "Message.h"

Message::Message(MessageType type) :
	type(type)
{
}

Message::Message(MessageType type, int value) :
	type(type), int_value(value)
{
}

Message::Message(MessageType type, float value, Vector vec_value) :
	type(type), float_value(value), vector_value(vec_value)
{
}

Message::Message(MessageType type, bool value) :
	type(type), bool_value(value)
{
}

MessageType Message::GetType()
{
	return type;
}

int Message::GetInt()
{
	return int_value;
}

void Message::SetInt(int value)
{
	int_value = value;
}

float Message::GetFloat()
{
	return float_value;
}

void Message::SetFloat(float value)
{
	float_value = value;
}

bool Message::GetBool()
{
	return bool_value;
}

void Message::SetBool(bool value)
{
	bool_value = value;
}

Vector & Message::GetVector()
{
	return vector_value;
}

void Message::SetVector(Vector value)
{
	vector_value = value;
}
